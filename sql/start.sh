docker stop pg-docker

docker run --rm -d --name pg-docker \
	-p 5432:5432 \
	-e POSTGRES_PASSWORD=docker \
	-v $pwd/sql:/docker-entrypoint-initdb.d \
	postgres
