**Board State**
```
{
	playOrder: {
		playerId: string;
		isTurn: boolean;
		isRoundStarter: boolean;
	}[],
	controlPoints: {
		team: number;
		x: number;
		y: number;
		z: number;
	}[],
	boardCoins: {

		playerId: number?;
		locationType: number; 

		location: {
			x: number;
			y: number;
			z: number;
		}


		coinType: number;
		count: number;

			}[],
	otherCoins: {
		playerId: string;
		coinType: number;
		count: number;
		location: number;
	}[],
	decrees: {
	    decreeType: number;
	    teams: number;
	}[]
}
```


```

available coins[]

turn: number;

player order: [] // player ids

playersSelectedCoins: []


```



**Teams**
```
{
    coyote = 0
    crow = 1,
}
```

**Coin Types**
```
{
    faceDown = 0,
    archer = 1,
    bannerman = 2,
    berserker = 3,
    bishop = 4,
    cavalry = 5,
    crossbowman = 6,
    earl = 7,
    ensign = 8,
    footman = 9,
    herald = 10,
    knight = 11,
    lancer = 12,
    lightCavalry = 13,
    marshall = 14,
    mercenary = 15,
    pikeman = 16,
    royalGuard = 17,
    scout = 18,
    swordsman = 19,
    warriorPriest = 20
}
```

**Royal Decree Types**
```
{
    none = 0,
    enlist = 1,
    guard = 2,
    march = 3,
    redeploy = 4,
    reinforce = 5,
    sacrifice = 6,
    spy = 7
}
```

**'Other' Locations**
```
{
    exile = 0,
    bag = 1,
    hand = 2,
    discardUp = 3,
    discardDown = 4
}
```
