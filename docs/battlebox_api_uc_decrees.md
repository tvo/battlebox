Royal Decrees
1. Enlist
2. Guard
3. March
4. Redeploy
5. Reinforce
6. Sacrifice
7. Spy

**Enlist**
1. client: request coin location update { player id, coin type, location (supply), location (discard) }
2. server: request result { is legal, options }
3. server (broadcast): coin location update { player id, coin type, location (supply), location (discard) }
4. client: request coin location update { player id, coin type, location (supply), location (discard) }
5. server: request result { is legal }
6. server (broadcast): coin location update { player id, coin type, location (supply), location (discard) }

**Guard**
1. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
    * this call should specify the friendly unit that will perform the attack
    * the two coordinates should be identical and on a friendly control point
2. server: request result { is legal, options }
3. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
    * this call should specify the target of the attack and be adjacent to the friendly unit
4. server: request result { is legal }
5. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }

**March**
1. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
    * target must be a friendly bolstered unit
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (coordinate), location (coordinate) }

**Redeploy**
1. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
    * target must be a friendly unit
    * second location can be any friendly control point
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (coordinate), location (coordinate) }

**Reinforce**
1. client: request coin location update { player id, coin type, location (exile), location (supply) }
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (exile), location (supply) }

**Sacrifice**
1. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
    * this call should specify the friendly unit that will perform the attack
    * the two coordinates should be identical
2. server: request result { is legal, options }
3. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
    * this call should specify the target of the attack and be adjacent to the friendly unit
4. server: request result { is legal }
5. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }
6. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }
    * second update is to remove attacking unit

**Spy**
1. client: request show hand { player id, player id }
    * second player id is target player
    * target must be an opponent
2. server: request result { is legal, [{coin type, quantity}] }
3. server (broadcast): show hand { player id, player id }
4. client: request coin location update { player id, coin type, location (hand), location (discard) }
5. server: request result { is legal }
6. server (broadcast): coin location update { player id, coin type, location (hand), location (discard) }
7. client (other): request draw { player id, quantity (1) }
8. server: draw result [ { coin type, quantity } ]

-or-

4. client: request pass { player id }
5. server: request result { is legal }
