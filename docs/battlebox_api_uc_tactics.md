Unit Types
1. Archer
2. Bannerman
3. Berserker
4. Bishop
5. Cavalry
6. Crossbowman
7. Earl
8. Ensign
9. Footman
10. Herald
11. Knight
12. Lancer
13. Light Cavalry
14. Marshall
15. Mercenary
16. Pikeman
17. Royal Guard
18. Scout
19. Swordsman
20. Warrior Priest

**Archer**
```
successful request to perform Archer tactic by player
```
1. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
    * target unit must be two spaces away from archer
    * prior options returned by server should reflect this
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }

**Bannerman**
```
legal request for a manuever using Bannerman is sent by player
```
1. server: request result { is legal, options }
    * this result (with options) is returned instead of normal request result
2. --server broadcasts update appropriate to maneuver--
3. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
4. server: request result { is legal }
5. server (broadcast): coin location update { player id, coin type, location (coordinate), location (coordinate) }

-or-

3. client: request pass { player id }
4. server: request result { is legal }

**Berserker**
```
legal request for a manever using bolstered Berserker is sent by player
```
1. server: request result { is legal, options }
    * this result (with options) is returned instead of normal request result
2. --server broadcasts update appropriate to maneuver--
3. client: request coin location update { player id, coin type (berserker), location (coordinate), location (discard) }
4. server: request result { is legal, options }
5. server (broadcast): coin location update { player id, coin type (berserker), location (coordinate), location (discard) }
6. --client requests another legal maneuver by Berserker--
    * if the Berserker unit is still bolstered, actions loop back to step 1
    * else a normal request result (with just an is legal flag) is returned

-or-

3. client: request pass { player id }
4. server: request result { is legal }

**Bishop**
```
successful request to perform Bishop tactic by player
```
1. client: request coin location update { player id, coin type, location (supply), location (discard) }
2. server: request result { is legal, options }
3. server (broadcast): coin location update { player id, coin type, location (supply), location (discard) }
4. client: request coin location update { player id, coin type, location (coordinate), location }
    * second location is exile if bishop attacked another coin
    * second location is a coordinate if bishop moved
5. server: request result { is legal }
6. server (broadcast): coin location update { player id, coin type, location (coordinate), location }

`options for attacking the bishop should not be returned by the server if the attacking unit is bolstered`

**Cavalry**
```
successful request to perform Cavalry tactic by player
```
1. client: request coin location update { player id, coin type (cavalry), location (coordinate), location (coordinate) }
    * second location must be adjacent to enemy unit
    * prior options returned by server should reflect this
2. server: request result { is legal, options }
3. server (broadcast): coin location update { player id, coin type (cavalry), location (coordinate), location (coordinate) }
4. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
5. server: request result { is legal }
6. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }

**Crossbowman**
```
successful request to perform Crossbowman tactic by player
```
1. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
    * target unit must be two spaces in straight line away from crossbowman and not obstructed by another coin
    * prior options returned by server should reflect this
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }

**Earl**
```
legal request to deploy Earl by player
```
1. server: request result { is legal, options }
    * this result (with options) is returned instead of normal request result
2. server (broadcast): coin location update { player id, coin type (earl), location (hand), location (coordinate) }
3. client: request coin location update { player id, coin type (earl), location (coordinate), location (coordinate) }
4. server: request result { is legal }
5. server (broadcast): coin location update { player id, coin type (earl), location (coordinate), location (coordinate) }

-or-

3. client: request pass { player id }
4. server: request result { is legal }

```
legal request to perform tactic by earl
```
1. server: request result { is legal, options }
    * this result (with options) is returned instead of normal request result
2. server (broadcast): point control { player id, location (coordinate) }
    * check Control use case for win-con check
3. client: request proclaim { player id, decree id }
4. server: update result { is legal }
5. server (broadcast): decree update { player id, decree id }
6. --server waits, returns, and broadcasts while all actions required for decree are resolved--

-or-

3. client: request pass { player id }
4. server: request result { is legal }

**Ensign**
```
successful request to perform Ensign tactic by player
```
1. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
    * target (coin type/coordinate) must be friendly unit within two spaces of ensign
    * second location must be legal move for target unit that is also within two spaces of ensign
    * prior options returned by server should reflect this
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (coordinate), location (coordinate) }

**Footman**
```
successful request to perform Footman tactic with two footmen on the board
```
1. --client requests legal maneuver with footman--
2. server: request result { is legal, options }
3. --server broadcasts update appropriate to maneuver--
4. --client requests legal maneuver with second footman--
5. server: request result { is legal }
6. --server broadcasts update appropriate to maneuver--

**Herald**
```
successful request to perform Herald tactic by player
```
1. client: request coin location update { player id, coin type, location (supply), location (coordinate) }
    * can only be used to bolster an unbolstered friendly unit next to the herald
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (supply), location (coordinate) }

```
player with a herald on the board successfully proclaims
```
1. --client requests legal maneuver with herald--
2. server: request result { is legal }
3. --server broadcasts update appropriate to maneuver--

-or-

3. client: request pass { player id }
4. server: request result { is legal }

**Knight**

`options for attacking the knight should not be returned by the server if the attacking unit is not bolstered`

**Lancer**
```
successful request to perform Lancer tactic by player
```
1. client: request coin location update { player id, coin type (lancer), location (coordinate), location (coordinate) }
    * second location must one or two spaces from the lancer's current location in a straight line
    * the next space further out from the second location must contain an enemy unit
    * prior options returned by server should reflect this
2. server: request result { is legal, options }
3. server (broadcast): coin location update { player id, coin type (lancer), location (coordinate), location (coordinate) }
4. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
    * the coin specified must be the enemy unit one space out from the lancer's new location
    * prior options returned by server should reflect this
5. server: request result { is legal }
6. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }

**Light Cavalry**
```
successful request to perform Light Cavalry tactic by player
```
1. client: request coin location update { player id, coin type (light cavalry), location (coordinate), location (coordinate) }
2. server: request result { is legal, options }
3. server (broadcast): coin location update { player id, coin type (light cavalry), location (coordinate), location (coordinate) }
4. client: request coin location update { player id, coin type (light cavalry), location (coordinate), location (coordinate) }
5. server: request result { is legal, options }
6. server (broadcast): coin location update { player id, coin type (light cavalry), location (coordinate), location (coordinate) }

**Marshall**
```
successful request to perform Marshall tactic by player
```
1. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
    * this call should specify the friendly unit that will perform the attack
    * the two coordinates should be identical and within two spaces of the marshall
2. server: request result { is legal, options }
3. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
    * this call should specify the target of the attack and be adjacent to the friendly unit
4. server: request result { is legal }
5. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }

**Mercenary**
```
successful request to recruit mercenary by player with a mercenary unit is on the board
```
1. --client requests legal maneuver with mercenary--
2. server: request result { is legal, options }
3. --server broadcasts update appropriate to maneuver--

-or-

3. client: request pass { player id }
4. server: request result { is legal }

**Pikeman**
```
successful request to attack pikeman by player using a unit adjacent to said pikeman
```
1. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }
    * specified coin is the attacking unit

**Royal Guard**
```
successful request to perform Royal Guard tactic by player
```
1. client: request coin location update { player id, coin type (royal guard), location (coordinate), location (coordinate) }
    * second coordinate must be a point controlled by the friendly team within 2 spaces of the royal guard's current location
    * prior options returned by server should reflect this
2. server: request result { is legal, options }
3. server (broadcast): coin location update { player id, coin type (royal guard), location (coordinate), location (coordinate) }

```
broadcast of attack targeting Royal Guard unit by other player
```
1. client: request coin location update { player id, coin type, location (supply), location (exile) }
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (supply), location (exile) }

-or-

1. client: request pass { player id }
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }

**Scout**

`options provided by server when deploying a scout should include the standard friendly control points and all spaces adjacent to friendly units`

**Swordsman**
```
successful request to attack with a swordsman by player
```
1. client: request coin location update { player id, coin type (swordsman), location (coordinate), location (coordinate) }
2. server: request result { is legal }
3. server (broadcast): coin location update { player id, coin type (swordsman), location (coordinate), location (coordinate) }-or-

-or-

1. client: request pass { player id }
2. server: request result { is legal }

**Warrior Priest**
```
successful request to attack or control with a warrior priest by player
```
1. client: request draw { player id, quantity (1) }
2. server: draw result [ { coin type, quantity } ]
3. --client requests to take any legal action with coin that was just drawn--
4. server: request result { is legal }
5. --server broadcasts update appropriate to action (if any)--