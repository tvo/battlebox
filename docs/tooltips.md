> The Marshall's tactic can only command units to perform a normal **attack**. This means units like the Archer, which cannot perform a normal **attack**, are not valid targets for this tactic. This also holds true for the Guard and Sacrifice royal decrees.

> The coin lost when attacking a Pikeman is not the result of an **attack**. Because of this, units like the Knight will still lose a coin when attacking the Pikeman even if the Knight could not be legally attacked by said unit.

> Because the Spy decree does not require that the target discard a coin, a player may target an opponent with no coins left in hand. This can be useful if a **proclaim** action can offer an advantage through unit abilities.

> If a player has 2 or less coins total between their hand, discard, and bag, they can only draw up to and play those two coins for the round. After they have played out their coins, their opponent will continue to play until their hand is empty.

> The bolster coin must be removed from the Berserker *before* it performs an additional **maneuver**. This action order can be relevant against units like the Bishop which cannot be attacked by bolstered units.