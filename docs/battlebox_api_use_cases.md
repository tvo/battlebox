Available Actions:
Face Down:
* Pass
* Take Initiative
* Recruit

Face Up Field:
* Deploy
* Bolster

Face Up Discard
* Control
* Move
* Attack
* Tactic
* Proclaim

Round Start

1. server (broadcast): start round notice { round #, turn order }
1. client: request draw { player id, quantity (3) }
1. server: draw result [ { coin type, quantity } ]
    * server waits for all draw requests to finish
1. server (broadcast): player turn start notice { player id }


Pass Turn

1. client: request pass { player id, coin type }
1. server: request result { is legal }
1. server (broadcast): coin location update { player id, coin type (face down), location (hand), location (discard) }


Take Initiative Turn

1. client: request take initiative { player id, coin type }
1. server: request result { is legal }
1. server (broadcast): coin location update { player id, coin type (face down), location (hand), location (discard) }
1. server (broadcast): initiative marker update { player id }


Recruit Turn

1. client: request recruit { player id, coin type }
1. server: request result { is legal, options }
1. server (broadcast): coin location update { player id, coin type, location (hand), location (discard) }
1. client: request coin location update { player id, coin type (face down), location (supply), location (discard) }
1. server: request result { is legal }
1. server (broadcast): coin location update { player id, coin type, location (supply), location (discard) }
    * if recruit results in option for action, refer steps 3+ of turn with corresponding action


Deploy

1. client: request deploy { player id, coin type }
1. server: request result { is legal, options }
1. client: request coin location update { player id, coin type, location (hand), location (coordinate) }
1. server: request result { is legal }
1. server (broadcast): coin location update { player id, coin type, location (hand), location (coordinate) }


Reinforce

1. client: request reinforce { player id, coin type }
1. server: request result { is legal, options }
1. client: request coin location update { player id, coin type, location (hand), location (coordinate) }
1. server: request result { is legal }
1. server: (broadcast): coin location update { player id, coin type, location (hand), location (coordinate) }


Control Turn

1. client: request control { player id, coin type }
1. server: request result { is legal, options }
1. server (broadcast): coin location update { player id, coin type, location (hand), location (discard) }
1. client: request point control { player id, coordinate }
1. server: request result { is legal }
1. server (broadcast): point control { player id, coordinate }
    *
    ```
    if final required capture point for team
        server (broadcast): team win { team }
        results in game end...and possibly fireworks
    ```
1. --if control can result in additional action, refer to step 3+ of turn with corresponding action--


Move Turn

1. client: request move { player id, coin type }
1. server: request result { is legal, options }
1. server (broadcast): coin location update { player id, coin type, location (hand), location (discard) }
1. client: request coin location update { player id, coin type, location (coordinate), location (coordinate) }
1. server: request result { is legal }
1. server (broadcast): coin location update { player id, coin type, location (coordinate), location (coordinate) }
1. if move can result in additional action, refer to step 3+ of turn with corresponding action--


Attack Turn

1. client: request attack { player id, coin type }
1. server: request result { is legal, options }
1. server (broadcast): coin location update { player id, coin type, location (hand), location (discard) }
1. client: request coin location update { player id, coin type, location (coordinate), location (exile) }
1. server: request result { is legal }
1. --if attack requires feedback from other player, refer to tactic flow for other server broadcast options--
1. server (broadcast): coin location update { player id, coin type, location (coordinate), location (exile) }
    * only applies if player feedback was not required
1. --if attack can result in additional action, refer to step 3+ of turn with corresponding action--


Tactic Turn

1. client: request tactic { player id, coin type }
1. server: request result { is legal, options }
1. server (broadcast): coin location update { player id, coin type, location (hand), location (discard) }
1. --server waits, returns, and broadcasts while all actions required for tactic are resolved--
1. --server waits, returns, and broadcasts, while any actions triggered by actions made with tactic are resolved--


Proclaim Turn

1. client: request proclaim { player id }
1. server: move result { is legal }
1. server (broadcast): coin location update { player id, coin type (royal), location (hand), location (discard) }
1. client: request proclaim { player id, decree id }
1. server: update result { is legal }
1. server (broadcast): decree update { player id, decree id }
1. --server waits, returns, and broadcasts while all actions required for decree are resolved--
1. --server waits, returns, and broadcasts, while any actions triggered by a decree are resolved--


To End Turn

1. server (broadcast): player turn start notice { player id }
    * start of next turn
    * or for last turn, round start notice


