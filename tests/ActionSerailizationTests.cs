﻿using System;
using Xunit;
using FluentAssertions;
using Api.Model;
using Api.Model.Actions;
using Api.Model.Positioning;

namespace tests
{
    public class ActionSerializationTests
    {
		[Fact]
        public void CanDeserializeDeploy()
        {
			// Given
			var position = new Cube(1, 2, 3);
			var player = new Gamer(1, 2, Team.Wolf, null);
			var deploy = new Deploy(position, CoinLocation.Hand, CoinType.Cavalry, player, DateTime.Now);
			var content = deploy.Serialize();

			// When
			var action = Deploy.Deserialize(content, player, DateTime.Now);

			// Then
			action.Location.Should().Be(CoinLocation.Hand);
			action.CoinUsed.Should().Be(CoinType.Cavalry);
			action.To.X.Should().Be(1);
			action.To.Y.Should().Be(2);
			action.To.Z.Should().Be(3);
		}

		[Fact]
        public void CanDeserializeMoveInBoard()
        {
			// Given
			var from = new Cube(1, 2, 3);
			var to = new Cube(5, 5, 5);
			var player = new Gamer(1, 2, Team.Wolf, null);
			var deploy = new MoveInBoard(from, to, player, DateTime.Now);
			var content = deploy.Serialize();

			// When
			var action = MoveInBoard.Deserialize(content, player, DateTime.Now);

			// Then
			action.From.X.Should().Be(1);
			action.From.Y.Should().Be(2);
			action.From.Z.Should().Be(3);

			action.To.X.Should().Be(5);
			action.To.Y.Should().Be(5);
			action.To.Z.Should().Be(5);
		}

		[Fact]
        public void CanDeserializeControl()
        {
			// Given
			var from = new Cube(1, 2, 3);
			var player = new Gamer(1, 2, Team.Wolf, null);
			var deploy = new Control(from, player, DateTime.Now);
			var content = deploy.Serialize();

			// When
			var action = Control.Deserialize(content, player, DateTime.Now);

			// Then
			action.Position.X.Should().Be(1);
			action.Position.Y.Should().Be(2);
			action.Position.Z.Should().Be(3);
		}

		[Fact]
        public void CanDeserializeRemove()
        {
			// Given
			var from = new Cube(1, 2, 3);
			var to = CoinLocation.Supply;
			var player = new Gamer(1, 2, Team.Wolf, null);
			var remove = new Remove(from, to, player, DateTime.Now);
			var content = remove.Serialize();

			// When
			var action = Remove.Deserialize(content, player, DateTime.Now);

			// Then
			action.From.X.Should().Be(1);
			action.From.Y.Should().Be(2);
			action.From.Z.Should().Be(3);
			action.To.Should().Be(CoinLocation.Supply);
		}
    }
}
