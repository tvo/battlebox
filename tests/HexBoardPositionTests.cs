﻿using System.Linq;
using Xunit;
using FluentAssertions;
using Api;
using Model = Api.Model;
using Pos = Api.Model.Positioning;
using dto = Api.dto;
using db = Api.db;
using System;

namespace tests
{
    public class HexBoardPositionTests
    {
		[Fact]
		public void TwoPlayerBoard_ShouldHaveXControlPoints()
		{
			// Given
			var board =  Model.Game.TwoPlayerBoard;

			// When
			var count = board.Where(x => x.Control).Count();

			// Then
			count.Should().Be(10);
		}

		[Fact]
		public void FourPlayerBoard_ShouldHaveXControlPoints()
		{
			// Given
			var board =  Model.Game.FourPlayerBoard;

			// When
			var count = board.Where(x => x.Control).Count();

			// Then
			count.Should().Be(14);
		}

    }
}
