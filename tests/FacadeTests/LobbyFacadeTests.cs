﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Immutable;
using FluentAssertions;
using Xunit;
using Moq;
using System;
using AutoMapper;
using Api.Repositories.Lobby;
using Api.Repositories.Player;
using Api.Services;
using Api.Facades;
using Model = Api.Model;
using dto = Api.dto;
using ApiNamespace = Api;

namespace tests.FacadeTests
{
	public class LobbyFacadeTests
	{
		private readonly LobbyFacade facade;

		private readonly ILobbyRepository lobbyRepository;
		private readonly IPlayerRepository playerRepository;
		private readonly IUserInfoProvider userInfo;

		public LobbyFacadeTests()
		{
			var config = new MapperConfiguration(x => x.AddProfile(new ApiNamespace.Map()));
			var mapper = config.CreateMapper();

			lobbyRepository = Mock.Of<ILobbyRepository>();
			playerRepository = Mock.Of<IPlayerRepository>();
			userInfo = Mock.Of<IUserInfoProvider>();
			Mock.Get(userInfo).Setup(x => x.GetUserId()).Returns(1);

			facade = new LobbyFacade(lobbyRepository, playerRepository, mapper, userInfo);
		}

		[Fact]
		public async Task LobbyFacadeCreate_GameType_TwoPlayerIsReturned()
		{
			// Given
			var toCreate = new dto.LobbyCreate();
			toCreate.Name = "to create";
			toCreate.GameType = Model.GameType.TwoPlayer;
			Mock.Get(lobbyRepository).Setup(x => x.Create(It.IsAny<Model.Lobby>()))
				.ReturnsAsync(new Model.Lobby(1, "created", Model.GameType.TwoPlayer, new int[]{}, new int[]{}, DateTime.UtcNow, 1));
			Mock.Get(playerRepository).Setup(x => x.GetPlayers(It.IsAny<IEnumerable<int>>()))
				.ReturnsAsync(ImmutableDictionary<int, Model.Player>.Empty);

			// When
			var created = await facade.Create(toCreate);

			// Then
			created.GameType.Should().Be(Model.GameType.TwoPlayer);
		}

		[Fact]
		public async Task LobbyFacadeCreate_GameType_FourPlayerIsReturned()
		{
			// Given
			var toCreate = new dto.LobbyCreate();
			toCreate.Name = "to create";
			toCreate.GameType = Model.GameType.FourPlayer;
			Mock.Get(lobbyRepository).Setup(x => x.Create(It.IsAny<Model.Lobby>()))
				.ReturnsAsync(new Model.Lobby(1, "created", Model.GameType.FourPlayer, new int[]{}, new int[]{}, DateTime.UtcNow, 1));
			Mock.Get(playerRepository).Setup(x => x.GetPlayers(It.IsAny<IEnumerable<int>>()))
				.ReturnsAsync(ImmutableDictionary<int, Model.Player>.Empty);

			// When
			var created = await facade.Create(toCreate);

			// Then
			created.GameType.Should().Be(Model.GameType.FourPlayer);
		}
	}
}
