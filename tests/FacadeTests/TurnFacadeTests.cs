﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using FluentAssertions;
using Xunit;
using Moq;
using System;
using AutoMapper;
using Api.Repositories.Action;
using Api.Repositories.Game;
using Api.Services;
using Api.Facades;
using Api.dto.Responses;
using Model = Api.Model;
using ApiNamespace = Api;

namespace tests.FacadeTests
{
	public class TurnFacadeTests
	{
		private readonly TurnFacade facade;

		private readonly ITurnRepository turnRepository;
		private readonly IGameRepository gameRepository;
		private readonly IUserInfoProvider userInfo;

		public TurnFacadeTests()
		{
			var config = new MapperConfiguration(x => x.AddProfile(new ApiNamespace.Map()));
			var mapper = config.CreateMapper();

			turnRepository = Mock.Of<ITurnRepository>();
			gameRepository = Mock.Of<IGameRepository>();
			userInfo = Mock.Of<IUserInfoProvider>();

			Mock.Get(userInfo).Setup(x => x.GetUserId()).Returns(1);

			facade = new TurnFacade(turnRepository, mapper, gameRepository, userInfo);
		}

		[Fact]
		public async Task TurnFacade_HistoryReturnsTurns()
		{
			// Given
			Mock.Get(turnRepository).Setup(x => x.GameHistory(1))
				.ReturnsAsync(new List<Model.Turn> { new Model.Turn(1, 2, 3, new []{new Model.Actions.TurnEnd(1, default, default)})});

			// When
			var history = await facade.History(1);

			// Then
			history.Should().ContainSingle();
			var h = history.Single();
		}

		[Fact]
		public async Task TurnFacade_EndTurn_SavesTurnToRepository()
		{
			// Given
			var playerIds = new []{1, 2, 3, 4};
			var gamers = playerIds.Select(x => new Model.Gamer(1, 1, Model.Team.Wolf, null, x)).ToList(); // every team is wolf I guess...

			Mock.Get(turnRepository).Setup(x => x.SaveTurn(1, It.IsAny<Model.Turn>())).ReturnsAsync(new ContentResponse<bool>(true));
			Mock.Get(turnRepository).Setup(x => x.GameHistory(1)).ReturnsAsync(new List<Model.Turn>());
			Mock.Get(turnRepository).Setup(x => x.GetMostRecentTurn(1)).ReturnsAsync(new Model.Turn(4, 0, 0, Array.Empty<Model.Actions.Action>()));
			Mock.Get(gameRepository).Setup(x => x.GetGame(1)).ReturnsAsync(new Model.Game(1, "ggame", gamers, DateTime.Now, 4));

			// When
			var result = await facade.EndTurn(1);

			// Then
			result.IsSuccess.Should().BeTrue();
			Mock.Get(turnRepository).Verify(x => x.SaveTurn(1, It.IsAny<Model.Turn>()), Times.Once);
		}

		[Fact]
		public async Task TurnFacade_EndTurn_AddsAnEndTurnToModel()
		{
			// Given
			var playerIds = new []{1, 2, 3, 4};
			var gamers = playerIds.Select(x => new Model.Gamer(1, 1, Model.Team.Wolf, null, x)).ToList();
			var now = DateTime.UtcNow;

			Mock.Get(turnRepository).Setup(x => x.GetMostRecentTurn(1)).ReturnsAsync(new Model.Turn(4, 0, 0, Array.Empty<Model.Actions.Action>()));
			Mock.Get(turnRepository).Setup(x => x.GameHistory(1)).ReturnsAsync(new List<Model.Turn>());
			Mock.Get(gameRepository).Setup(x => x.GetGame(1)).ReturnsAsync(new Model.Game(1, "ggame", gamers, now, 4));
			Model.Turn savedTurn = null;
			Mock.Get(turnRepository).Setup(x => x.SaveTurn(1, It.IsAny<Model.Turn>())).ReturnsAsync(new ContentResponse<bool>(true))
				.Callback((int id, Model.Turn sTurn) => savedTurn = sTurn);

			// When
			var result = await facade.EndTurn(1);

			// Then
			result.IsSuccess.Should().BeTrue();
			savedTurn.Should().NotBeNull();
			var change = savedTurn.StateChanges.Single();
			change.Time.Should().NotBeBefore(DateTime.Parse("2000-1-1")); // make sure you clock is not set 20+ years back.
		}
	}
}
