﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using FluentAssertions;
using Api;
using db = Api.db;

namespace tests.Api
{

	public class VersionTests
	{
		private readonly HttpClient http;

		public VersionTests()
		{
			var host = new TestServer(new WebHostBuilder()
					.UseEnvironment("test")
					.UseStartup<Startup>()
					.ConfigureServices(services => {
						services.AddDbContext<db.DataContext>(options => options.UseInMemoryDatabase(Guid.NewGuid().ToString()));
						})
					);
			http = host.CreateClient();
		}

		[Fact]
		public async Task Version_Ok_StatusCode()
		{
			// Given
			var url = "/api/version";

			// When
			var response = await http.GetAsync(url);

			// Then
			response.StatusCode.Should().Be(HttpStatusCode.OK);
		}
	}
}
