﻿using System;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Xunit;
using FluentAssertions;
using Api;
using db = Api.db;
using dto = Api.dto;
using Model = Api.Model;

namespace tests.Api
{

	public class LobbyTests
	{
		private readonly HttpClient http;
		private const string baseUrl = "/api/lobby";

		public LobbyTests()
		{
			var host = new TestServer(new WebHostBuilder()
					.UseEnvironment("test")
					.UseStartup<Startup>()
					.ConfigureServices(services => {
						var options = new DbContextOptionsBuilder<db.DataContext>().UseInMemoryDatabase(Guid.NewGuid().ToString());
						services.AddSingleton(new db.DataContext(options.Options));
						})
					);
			http = host.CreateClient();
			http.DefaultRequestHeaders.Add("playerId", "1");
		}

		[Fact]
		public async Task GetLobby_ShouldBeEmpty()
		{
			// Given
			// When
			var response = await http.GetAsync(baseUrl);

			// Then
			response.StatusCode.Should().Be(HttpStatusCode.OK);
			var json = JsonConvert.DeserializeObject<List<dto.Lobby>>(await response.Content.ReadAsStringAsync());
			json.Should().BeEmpty();
		}

		[Fact]
		public async Task CreateLobby_ReturnsCreatedLobby()
		{
			// Given
			var lobbyToCreate = new dto.LobbyCreate();
			lobbyToCreate.Name = "my lobby";
			lobbyToCreate.GameType = Model.GameType.TwoPlayer;

			// When
			var response = await http.PostAsync(baseUrl, new StringContent(JsonConvert.SerializeObject(lobbyToCreate), Encoding.UTF8, "application/json"));

			// Then
			response.StatusCode.Should().Be(HttpStatusCode.Created);
			var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dto.Lobby>(await response.Content.ReadAsStringAsync());
			json.Should().NotBeNull();
		}

		[Fact]
		public async Task CreateLobby_WithTeamA_ReturnsCreatedLobby()
		{
			// Given
			var mePlayer = new dto.Player();
			var lobbyToCreate = new dto.LobbyCreate();
			lobbyToCreate.Name = "my lobby";
			lobbyToCreate.GameType = Model.GameType.TwoPlayer;
			lobbyToCreate.TeamA = new List<int> { 1 };

			// When
			var playerResponse = await http.PostAsync("/api/player", new StringContent(JsonConvert.SerializeObject(mePlayer), Encoding.UTF8, "application/json"));
			playerResponse.StatusCode.Should().Be(HttpStatusCode.OK);

			var response = await http.PostAsync(baseUrl, new StringContent(JsonConvert.SerializeObject(lobbyToCreate), Encoding.UTF8, "application/json"));

			// Then
			response.StatusCode.Should().Be(HttpStatusCode.Created);
			var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dto.Lobby>(await response.Content.ReadAsStringAsync());
			json.Should().NotBeNull();
			json.TeamA.Single().Id.Should().Be(1);
			json.TeamB.Should().BeEmpty();
		}

		[Fact]
		public async Task CreateLobby_GameType_TwoPlayer_Accepted()
		{
			// Given
			var mePlayer = new dto.Player();
			var lobbyToCreate = new dto.LobbyCreate();
			lobbyToCreate.Name = "twoPlayerLobby";
			lobbyToCreate.GameType = Model.GameType.TwoPlayer;
			lobbyToCreate.TeamA = new List<int> { 1 };
			var playerResponse = await http.PostAsync("/api/player", new StringContent(JsonConvert.SerializeObject(mePlayer), Encoding.UTF8, "application/json"));

			// When
			var response = await http.PostAsync(baseUrl, new StringContent(JsonConvert.SerializeObject(lobbyToCreate), Encoding.UTF8, "application/json"));

			// Then
			response.StatusCode.Should().Be(HttpStatusCode.Created);
			var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dto.Lobby>(await response.Content.ReadAsStringAsync());
			json.GameType.Should().Be(Model.GameType.TwoPlayer);
		}

		[Fact]
		public async Task CreateLobby_GameType_FourPlayer_Accepted()
		{
			// Given
			var mePlayer = new dto.Player();
			var lobbyToCreate = new dto.LobbyCreate();
			lobbyToCreate.Name = "FourPlayerLobby";
			lobbyToCreate.GameType = Model.GameType.FourPlayer;
			lobbyToCreate.TeamA = new List<int> { 1 };
			var playerResponse = await http.PostAsync("/api/player", new StringContent(JsonConvert.SerializeObject(mePlayer), Encoding.UTF8, "application/json"));

			// When
			var response = await http.PostAsync(baseUrl, new StringContent(JsonConvert.SerializeObject(lobbyToCreate), Encoding.UTF8, "application/json"));

			// Then
			response.StatusCode.Should().Be(HttpStatusCode.Created);
			var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dto.Lobby>(await response.Content.ReadAsStringAsync());
			json.GameType.Should().Be(Model.GameType.FourPlayer);
		}

		[Fact]
		public async Task DeleteLobby_ReturnsObjectWithIdProperty()
		{
			// Given
			var lobbyToCreate = new dto.LobbyCreate();
			lobbyToCreate.Name = "my lobby";
			await http.PostAsync(baseUrl, new StringContent(JsonConvert.SerializeObject(lobbyToCreate), Encoding.UTF8, "application/json"));

			// When
			var response = await http.DeleteAsync(baseUrl + "/1");

			// Then
			response.StatusCode.Should().Be(HttpStatusCode.OK);
			var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dto.DeletedDto>(await response.Content.ReadAsStringAsync());
			json.Should().NotBeNull();
			json.Id.Should().Be(1);
		}
	}
}
