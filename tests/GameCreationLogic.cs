﻿using System;
using Xunit;
using FluentAssertions;
using Api.Model;

namespace tests
{

    public class GameModelTests
    {
        [Fact]
        public void CanCreateAGameWithFourPlayers()
        {
 			// Given
			var playerA = new Gamer(3, 0, Team.Wolf, null);
			var playerB = new Gamer(4, 0, Team.Raven, null);
			var playerC = new Gamer(5, 0, Team.Wolf, null);
			var playerD = new Gamer(6, 0, Team.Raven, null);

			// When
			var game = new Game(1, "", new [] { playerA, playerB, playerC, playerD }, DateTime.UtcNow, 1);

			// Then
			game.Players.Should().HaveCount(4);
       }

        [Fact]
        public void CanCreateAGameWithTwoPlayers()
        {
 			// Given
			var playerA = new Gamer(3, 0, Team.Wolf, null);
			var playerB = new Gamer(4, 0, Team.Raven, null);

			// When
			var game = new Game(1, "", new [] { playerA, playerB, }, DateTime.UtcNow, 1);

			// Then
			game.Players.Should().HaveCount(2);
       }

        [Fact]
        public void CanNotCreateAGameWithOnePlayer()
        {
 			// Given
			var playerA = new Gamer(3, 0, Team.Wolf, null);

			// When
			var valid = new Game(1, "", new [] { playerA, }, DateTime.UtcNow, 1).ValidPlayers();

			// Then
			valid.Should().BeFalse();
       }

        [Fact]
        public void CanNotCreateAGameWithThreePlayers()
        {
 			// Given
			var playerA = new Gamer(3, 0, Team.Wolf, null);
			var playerB = new Gamer(4, 0, Team.Raven, null);
			var playerC = new Gamer(5, 0, Team.Wolf, null);

			// When
			var valid = new Game(1, "", new [] { playerA, }, DateTime.UtcNow, 1).ValidPlayers();

			// Then
			valid.Should().BeFalse();
		}

		[Fact]
		public void CanNotCreateAGameWithFivePlayers()
		{
 			// Given
			var playerA = new Gamer(3, 0, Team.Wolf, null);
			var playerB = new Gamer(4, 0, Team.Raven, null);
			var playerC = new Gamer(5, 0, Team.Wolf, null);
			var playerD = new Gamer(6, 0, Team.Raven, null);
			var playerZ = new Gamer(7, 0, Team.Wolf, null);

			// When
			var valid = new Game(1, "", new [] { playerA, }, DateTime.UtcNow, 1).ValidPlayers();

			// Then
			valid.Should().BeFalse();
		}

		[Fact]
        public void PickCardOnFirstRound()
        {
 			// Given

			// When

			// Then

       }

		[Fact]
        public void FirstRoundWithTwoPlayersAlternate()
        {
 			// Given

			// When

			// Then

       }

		[Fact]
        public void FirstRoundWithTwoPlayersEndsWhenPlayersHaveThreeCards()
        {
 			// Given

			// When

			// Then

       }

		[Fact]
        public void FirstRoundWithFourPlayers_HasCorrectOrder()
        {
			// Given

			// When

			// Then

        }

    }
}
