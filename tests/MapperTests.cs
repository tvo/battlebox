﻿using System.Linq;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using AutoMapper;
using Api;
using Model = Api.Model;
using dto = Api.dto;
using db = Api.db;
using System;

namespace tests
{
    public class MapperTests
    {
		private readonly IMapper mapper;

		public MapperTests()
		{
			var config = new MapperConfiguration(x => x.AddProfile(new Map()));
			mapper = config.CreateMapper();
		}

		[Fact]
		public void BoardPosition_to_HexDto()
		{
			// Given
			var cube = new Model.Positioning.Cube(1, 2, 3);
			var model = new Model.Positioning.BoardPosition<Model.Positioning.Cube>(cube, true);

			// When
			var dto = mapper.Map<dto.HexPosition>(model);

			// Then
			dto.X.Should().Be(1);
			dto.Y.Should().Be(2);
			dto.Z.Should().Be(3);
			dto.Control.Should().BeTrue();
		}

		[Fact]
		public void CanMapTheBoardPosition()
		{
			// Given
			var model = Model.Game.FourPlayerBoard;

			// When
			var dto = mapper.Map<List<dto.HexPosition>>(model);

			// Then
			dto.Should().NotBeEmpty();
		}

		[Fact]
		public void HexDto_to_BoardPosition()
		{
			// Given
			var dto = new dto.HexPosition() { X = 1, Y = 2, Z = 3, Control = true };

			// When
			var model = mapper.Map<Model.Positioning.BoardPosition<Model.Positioning.Cube>>(dto);

			// Then
			model.Position.X.Should().Be(1);
			model.Position.Y.Should().Be(2);
			model.Position.Z.Should().Be(3);
			model.Control.Should().BeTrue();
		}

        [Fact]
        public void LobbyModel_To_LobbyDto()
        {
			// Given
			var model = new Model.Lobby(2, "model name", Model.GameType.FourPlayer, new []{1}, new []{2}, DateTime.UtcNow, 1);

			// When
			var dto = mapper.Map<dto.Lobby>(model);

			// Then
			dto.Id.Should().Be(2);
			dto.Name.Should().Be("model name");
			dto.GameType.Should().Be(Model.GameType.FourPlayer);
			dto.TeamA.Should().BeNullOrEmpty();
			dto.TeamB.Should().BeNullOrEmpty();
		}

        [Fact]
        public void LobbyCreateDto_To_LobbyModel()
        {
			// Given
			var createLobbyDto = new dto.LobbyCreate()
			{
				Name = "create name",
				GameType = Model.GameType.FourPlayer,
				TeamA = new []{ 1 }.ToList(),
				TeamB = new []{ 2 }.ToList()
			};

			// When
			var model = mapper.Map<Model.Lobby>(createLobbyDto);

			// Then
			model.Id.Should().Be(0);
			model.Name.Should().Be("create name");
			model.RequiredPlayers.Should().Be(4);
			model.TeamAPlayerIds.Should().Contain(1);
			model.TeamBPlayerIds.Should().Contain(2);
		}

		[Fact]
		public void Gamer_DbToModel()
		{
			// Give
			var gamer = new db.PlayerInGame();
			gamer.GameId = 1;
			gamer.DrawCard(1);
			gamer.DrawCard(2);
			gamer.DrawCard(3);
			gamer.DrawCard(4);
			gamer.Team = (int)Model.Team.Raven;
			gamer.PlayerId = 1;

			// When
			var entity = mapper.Map<Model.Gamer>(gamer);

			// Then
			entity.PlayerId.Should().Be(1);
			entity.Team.Should().Be((int)Model.Team.Raven);
			entity.Coins.Should().Contain((Model.CoinType)1);
			entity.Coins.Should().Contain((Model.CoinType)2);
			entity.Coins.Should().Contain((Model.CoinType)3);
			entity.Coins.Should().Contain((Model.CoinType)4);
		}

		[Fact]
		public void Gamer_ModelToDb()
		{
			// Give
			var gamer = new Model.Gamer(1, 2, Model.Team.Raven, null, 0);

			// When
			var entity = mapper.Map<db.PlayerInGame>(gamer);

			// Then
			entity.PlayerId.Should().Be(1);
			entity.Team.Should().Be((int)Model.Team.Raven);
			entity.CardOne.Should().Be(-1);
			entity.CardTwo.Should().Be(-1);
			entity.CardThree.Should().Be(-1);
			entity.CardFour.Should().Be(-1);
		}
    }
}
