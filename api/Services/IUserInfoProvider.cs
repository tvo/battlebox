﻿namespace Api.Services
{
	public interface IUserInfoProvider
	{
		int? GetUserId();
	}
}
