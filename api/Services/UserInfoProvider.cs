﻿using Microsoft.AspNetCore.Http;

namespace Api.Services
{
	public class UserInfoProvider : IUserInfoProvider
	{
		private readonly IHttpContextAccessor contextAccessor;

		public UserInfoProvider(IHttpContextAccessor contextAccessor)
		{
			this.contextAccessor = contextAccessor;
		}


		public int? GetUserId()
		{
			var headers = contextAccessor.HttpContext.Request.Headers;
			if(headers.TryGetValue("playerId", out var pIdString) && int.TryParse(pIdString, out int pid))
				return pid;
			return null;
		}
	}
}
