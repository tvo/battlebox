﻿using Microsoft.AspNetCore.Mvc;
using Api.dto.Responses;

namespace Api.Services
{
	public static class FacadeResponseExtensions
	{
		public static ActionResult GetResponse(this FacadeResponse result)
		{
			if(result is ErrorResponse error)
				return new BadRequestObjectResult(error);

			if(result is IHaveContent content)
				return new OkObjectResult(content.GetContent());

			if(result is NoContentResponse)
				return new OkResult();

			return new BadRequestObjectResult("facade response not found. This wan't supposed to happen.");
		}
	}
}
