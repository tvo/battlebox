﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace Api.Facades
{
	using Repositories.Game;
	using Repositories.Lobby;
	using Services;

	public class GameFacade
	{
		private readonly IGameRepository Repository;
		private readonly ILobbyRepository LobbyRepository;
		private readonly IMapper Mapper;
		private readonly IUserInfoProvider UserProvider;

		public GameFacade(IGameRepository repository, ILobbyRepository lobbyRepository, IMapper mapper, IUserInfoProvider userInfoProvider)
		{
			Repository = repository;
			LobbyRepository = lobbyRepository;
			Mapper = mapper;
			UserProvider = userInfoProvider;
		}

		public async Task<List<dto.Game>> GetAll()
		{
			var dtos = Mapper.Map<List<dto.Game>>(await Repository.GetGames());
			foreach(var dto in dtos)
			{
				dto.PlayersTurnId = dto.Players.First().Id;
			}
			return dtos;
		}
		public async Task<dto.Game> GetOne(int gameId) 
		{
			var dto = Mapper.Map<dto.Game>(await Repository.GetGame(gameId));
			dto.PlayersTurnId = dto.Players.First().Id;
			return dto;
		}

		public async Task<dto.DeletedDto> Delete(int gameId) 
		{
			if(await Repository.Delete(gameId))
				return new dto.DeletedDto() { Id = gameId };
			return null;
		}

		public async Task<dto.Game> Create(dto.GameCreate gameCreate)
		{
			var lobby = await LobbyRepository.GetLobby(gameCreate.LobbyId);
			var gamers = new List<Model.Gamer>();

			foreach(var pId in lobby.TeamAPlayerIds)
				gamers.Add(new Model.Gamer(pId, 0, Model.Team.Wolf, null));
			foreach(var pId in lobby.TeamBPlayerIds)
				gamers.Add(new Model.Gamer(pId, 0, Model.Team.Raven, null));

			var game = new Model.Game(0, lobby.Name, gamers, DateTime.UtcNow, UserProvider.GetUserId().Value);
			// game = game.SortPlayers(); TODO: Saving this for later. Order should be implicit.
			if(!game.ValidPlayers())
				return null;
			var gameModel = await Repository.Create(Mapper.Map<Model.Game>(game));
			await LobbyRepository.Delete(gameCreate.LobbyId);
			
			return Mapper.Map<dto.Game>(gameModel);
		}
	

		public List<dto.Position> GetTwoPlayerPositions()
		{
			return Mapper.Map<List<dto.Position>>(Model.Game.TwoPlayerBoard.Select(x => Model.Positioning.Cube.ConverToCube(x)));
		}

		public List<dto.Position> GetFourPlayerPositions()
		{
			return Mapper.Map<List<dto.Position>>(Model.Game.FourPlayerBoard.Select(x => Model.Positioning.Cube.ConverToCube(x)));
		}
	}
}
