﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using Api.Repositories.Lobby;
using Api.Repositories.Player;

namespace Api.Facades
{
	using Services;

	public class LobbyFacade
	{
		private readonly ILobbyRepository Repository;
		private readonly IPlayerRepository PlayerRepository;
		private readonly IMapper Mapper;
		private readonly IUserInfoProvider userInfoProvider;

		public LobbyFacade(ILobbyRepository repository, IPlayerRepository playerRepository, IMapper mapper, IUserInfoProvider userInfoProvider)
		{
			Repository = repository;
			PlayerRepository = playerRepository;
			Mapper = mapper;
			this.userInfoProvider = userInfoProvider;
		}

		public async Task<List<dto.Lobby>> GetAll()
		{
			var lobby = await Repository.GetLobbies();
			var dtos = Mapper.Map<List<dto.Lobby>>(lobby);
			foreach (var dto in dtos)
			{
				var model = lobby.First(x => x.Id == dto.Id);
				await AddPlayers(dto, model);
			}
			return dtos;
		}

		public async Task<dto.Lobby> Get(int lobbyId)
		{
			var lobby = await Repository.GetLobby(lobbyId);

			var dto = Mapper.Map<dto.Lobby>(lobby);
			await AddPlayers(dto, lobby);

			return dto;
		}

		public async Task<dto.Lobby> Create(dto.LobbyCreate lobby)
		{
			// null here makes for exception in mapping :( .
			lobby.TeamA ??= new List<int>();
			lobby.TeamB ??= new List<int>();

			lobby.CreatedDate = DateTime.UtcNow;
			lobby.CreatedById =	userInfoProvider.GetUserId().Value;
			var model = Mapper.Map<Model.Lobby>(lobby);

			model = await Repository.Create(model);
			var dto = Mapper.Map<dto.Lobby>(model);
			dto.TeamA ??= new List<dto.Player>();
			dto.TeamB ??= new List<dto.Player>();

			await AddPlayers(dto, model);

			return dto;
		}

		public async Task<bool> Join(int lobbyId, Model.Team team)
		{
			var playerId = userInfoProvider.GetUserId() ?? -1;
			if (playerId == -1)
				return false;

			// player checks
			var player = await PlayerRepository.GetPlayer(playerId);
			if (player is null)
				return false;
			// lobby check logic
			var lobby = await Repository.GetLobby(lobbyId);
			if (lobby is null)
				return false;

			// game switch logic
			if (lobby.TeamAPlayerIds.Contains(playerId) || lobby.TeamBPlayerIds.Contains(playerId))
				await Repository.RemovePlayerToLobby(lobbyId, playerId);

			return await Repository.AddPlayerToLobby(lobbyId, playerId, team);
		}

		public async Task<bool> Leave(int lobbyId)
		{
			var playerId = userInfoProvider.GetUserId() ?? -1;
			return await Repository.RemovePlayerToLobby(lobbyId, playerId);
		}

		public async Task<bool> Delete(int lobbyId)
		{
			var playerId = userInfoProvider.GetUserId();
			return await Repository.Delete(lobbyId);
		}

		// note, this mutates the dto object. Not 100% happy with that.
		// this could also be optimized for get many players.
		private async Task AddPlayers(dto.Lobby dto, Model.Lobby model)
		{
			var ids = new List<int>();

			ids.AddRange(model.TeamAPlayerIds);
			ids.AddRange(model.TeamBPlayerIds);

			var players = await PlayerRepository.GetPlayers(ids);

			if(players is null)
				return;

			dto.TeamA ??= new List<dto.Player>();
			foreach(var id in model.TeamAPlayerIds)
			{
				if(players.ContainsKey(id))
					dto.TeamA.Add(Mapper.Map<dto.Player>(players[id]));
			}
			dto.TeamB ??= new List<dto.Player>();
			foreach(var id in model.TeamBPlayerIds)
			{
				if(players.ContainsKey(id))
					dto.TeamB.Add(Mapper.Map<dto.Player>(players[id]));
			}
		}
	}
}
