﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Immutable;
using AutoMapper;

namespace Api.Facades
{
	using Repositories.Action;
	using Repositories.Game;
	using Services;
	using dto.Responses;

	public class TurnFacade
	{
		private readonly ITurnRepository Repository;
		private readonly IGameRepository GameRepository;
		private readonly IMapper Mapper;
		private readonly IUserInfoProvider UserInfo;

		public TurnFacade(ITurnRepository repository, IMapper mapper, IGameRepository gameRepository, IUserInfoProvider userInfo)
		{
			Repository = repository;
			GameRepository = gameRepository;
			Mapper = mapper;
			UserInfo = userInfo;
		}

		public async Task<List<dto.Turn>> History(int gameId)
		{
			// TODO: some permission checks here.

			var history = await Repository.GameHistory(gameId);
			return history.SelectMany(turn => MapTurnsToDto(turn)).ToList();
		}

		private IEnumerable<dto.Turn> MapTurnsToDto(Model.Turn turn)
		{
			foreach(var change in turn.StateChanges)
			{
				var dto = new dto.Turn();
				dto.Action = change.Serialize();
				dto.Change = change;
				dto.PlayerId = change.Player?.PlayerId ?? -1;
				yield return dto;
			}
		}

		public async Task<dto.Player> NextPlayersTurn(int gameId)
		{
			var game = await GameRepository.GetGame(gameId);
			var turn = await Repository.GetMostRecentTurn(gameId);
			var gamer = NextPlayersTurn(game, turn);
			return Mapper.Map<dto.Player>(gamer);
		}

		private Model.Gamer NextPlayersTurn(Model.Game game, Model.Turn turn)
		{
			if (turn.RoundNumber == 0)
			{
				var gamerOrder = Model.Game.GetPlayerDrawOrder(game.Players.Select(x => x.PlayerId).ToList())
					.Select(x => game.GetPlayer(x))
					.Skip(turn.TurnNumber)
					.First().PlayerId;
			}

			var playersTurn = game.Players.First();
			if(game.Players.Any(x => x.PlayerId == game.PlayerIdsTurn))
				playersTurn = game.Players.First(x => x.PlayerId == game.PlayerIdsTurn);

			var iPlayersTurn = game.Players.IndexOf(playersTurn);
			var iNext = ((iPlayersTurn + 1) % game.Players.Count);
			var nextPlayersTurnId = game.Players[iNext].PlayerId;
			return game.GetPlayer(nextPlayersTurnId);
		}

		public async Task<FacadeResponse> PickCard(int gameId, Model.CoinType card)
		{
			var playerId = UserInfo.GetUserId();
			if (playerId is null) return new ErrorResponse("who are you?");

			var game = await GameRepository.GetGame(gameId);
			if (game is null) return new ErrorResponse("Game not found of Id: " + gameId);

			// check if the player isn't in the game.
			if (game.Players.All(x => x.PlayerId != playerId)) return new ErrorResponse("You aren't in that game!");

			if (game.Phase != Model.PhaseEnum.Drafting) return new ErrorResponse("Drafting phase is over bro.");

			// card can't be picked twice.
			if (game.Players.Any(x => x.Coins.Contains(card))) return new ErrorResponse("Card/CoinType is already picked.");

			var turn = await Repository.GetMostRecentTurn(gameId);

			if (turn is null)
				turn = new Model.Turn(playerId.Value, 0, 0, null);

			turn = turn.StartNextTurn(NextPlayersTurn(game, turn).PlayerId);

			// check the players turn. TODO: uncomment when we want more validation.
			//if(turn.PlayerId != playerId)
			//	return false;

			var action = new Model.Actions.PickCard(card, game.GetPlayer(playerId.Value), DateTime.UtcNow);

			if(!await DoesTurnBreakState(game, action))
				return new ErrorResponse("This action would break the game if applied. Rolling back.");


			var modifiedTurn = turn
				.AddStateChange(action)
				.AddStateChange(new Model.Actions.TurnEnd(playerId.Value, game.GetPlayer(playerId.Value), DateTime.UtcNow));

			// after turn end start the new turn?
			await GameRepository.DrawCard(game, playerId.Value, card);
			return await Repository.SaveTurn(gameId, modifiedTurn);
		}

		public async Task<FacadeResponse> Deploy(int gameId, dto.DeployDto deployDto)
		{
			var playerId = UserInfo.GetUserId();
			if (playerId is null) return new ErrorResponse("Who are you?");

			var game = await GameRepository.GetGame(gameId);
			if (game is null) return new ErrorResponse("Can't find the game.");

			// check if the player isn't in the game.
			if (game.Players.All(x => x.PlayerId != playerId)) return new ErrorResponse("You aren't in this game!");

			var turn = await Repository.GetMostRecentTurn(gameId);
			if (turn is null) return new ErrorResponse("Can't end the turn without doing any moves.");

			var to = Mapper.Map<Model.Positioning.Cube>(deployDto.To);
			var card = deployDto.Card;
			var location = deployDto.Location;

			var action = new Model.Actions.Deploy(to, location, card , game.GetPlayer(playerId.Value), DateTime.UtcNow);

			if(!await DoesTurnBreakState(game, action))
				return new ErrorResponse("This action would break the game if applied. Rolling back.");

			var modifiedTurn = turn.AddStateChange(action);

			return await Repository.SaveTurn(gameId, modifiedTurn);
		}

		public async Task<FacadeResponse> Move(int gameId, dto.MoveDto move)
		{
			var playerId = UserInfo.GetUserId();
			if (playerId is null) return new ErrorResponse("Who are you?");

			var game = await GameRepository.GetGame(gameId);
			if (game is null) return new ErrorResponse("Can't find the game.");

			// check if the player isn't in the game.
			if (game.Players.All(x => x.PlayerId != playerId)) return new ErrorResponse("You aren't in this game!");

			var turn = await Repository.GetMostRecentTurn(gameId);
			if (turn is null) return new ErrorResponse("Need to draft first before you can move a card.");

			var from = Mapper.Map<Model.Positioning.Cube>(move.From);
			var to = Mapper.Map<Model.Positioning.Cube>(move.To);

			var action = new Model.Actions.MoveInBoard(from, to, game.GetPlayer(playerId.Value), DateTime.UtcNow);

			if(!await DoesTurnBreakState(game, action))
				return new ErrorResponse("This action would break the game if applied. Rolling back.");

			var modifiedTurn = turn.AddStateChange(action);

			return await Repository.SaveTurn(gameId, modifiedTurn);
		}

		public async Task<FacadeResponse> Remove(int gameId, dto.RemoveDto remove)
		{
			var playerId = UserInfo.GetUserId();
			if (playerId is null) return new ErrorResponse("Who are you?");

			var game = await GameRepository.GetGame(gameId);
			if (game is null) return new ErrorResponse("Can't find the game.");

			// check if the player isn't in the game.
			if (game.Players.All(x => x.PlayerId != playerId)) return new ErrorResponse("You aren't in this game!");

			var turn = await Repository.GetMostRecentTurn(gameId);
			if (turn is null) return new ErrorResponse("Need to draft first before you can move a card.");

			var from = Mapper.Map<Model.Positioning.Cube>(remove.From);

			var action = new Model.Actions.Remove(from, remove.To, game.GetPlayer(playerId.Value), DateTime.UtcNow);

			if(!await DoesTurnBreakState(game, action))
				return new ErrorResponse("This action would break the game if applied. Rolling back.");

			var modifiedTurn = turn.AddStateChange(action);

			return await Repository.SaveTurn(gameId, modifiedTurn);
		}

		public async Task<FacadeResponse> Transfer(int gameId, dto.TransferDto dto)
		{
			var playerId = UserInfo.GetUserId();
			if (playerId is null) return new ErrorResponse("Who are you?");

			var game = await GameRepository.GetGame(gameId);
			if (game is null) return new ErrorResponse("Can't find the game.");

			// check if the player isn't in the game.
			if (game.Players.All(x => x.PlayerId != playerId)) return new ErrorResponse("You aren't in this game!");

			var turn = await Repository.GetMostRecentTurn(gameId);
			if (turn is null) return new ErrorResponse("Need to draft first before you can move a card.");

			var action = new Model.Actions.Transfer(dto.From, dto.To, dto.Type, dto.Amount, game.GetPlayer(playerId.Value), DateTime.UtcNow);

			if(!await DoesTurnBreakState(game, action))
				return new ErrorResponse("This action would break the game if applied. Rolling back.");

			var modifiedTurn = turn.AddStateChange(action);

			return await Repository.SaveTurn(gameId, modifiedTurn);
		}


		public async Task<FacadeResponse> EndTurn(int gameId)
		{
			var playerId = UserInfo.GetUserId();
			if (playerId is null) return new ErrorResponse("Who are you?");

			var game = await GameRepository.GetGame(gameId);
			if(game is null) return new ErrorResponse("Can't find the game.");

			// check if the player isn't in the game.
			if(game.Players.All(x => x.PlayerId != playerId)) return new ErrorResponse("You aren't in this game!");

			var turn = await Repository.GetMostRecentTurn(gameId);

			if (turn is null) // end turn will always have a non null turn to be valid. Not the case for pick card.
				return new ErrorResponse("Can't end the turn without doing any moves.");

			// check the players turn. TODO: uncomment when we want more validation.
			//if(turn.PlayerId != playerId)
			//	return false;

			var action = new Model.Actions.TurnEnd(playerId.Value, game.GetPlayer(playerId.Value), DateTime.UtcNow);

			if(!await DoesTurnBreakState(game, action))
				return new ErrorResponse("This action would break the game if applied. Rolling back.");

			var modifiedTurn = turn.AddStateChange(action);
			// after turn end start the new turn?

			return await Repository.SaveTurn(gameId, modifiedTurn);
		}

		public async Task<FacadeResponse> GetState(int gameId)
		{
			var game = await GameRepository.GetGame(gameId);
			var allTurns = await Repository.GameHistory(gameId);
			
			if(game is null)
				return new ErrorResponse("Game not found.");

			var state = game.ApplyState(allTurns.SelectMany(x => x.StateChanges));

			var stateDto = new dto.GameState();

			return ContentResponse.From(state);
		}

		private async Task<bool> DoesTurnBreakState(Model.Game game, Model.Actions.Action proposedAction)
		{
			var allTurns = await Repository.GameHistory(game.Id);

			if(game is null)
				return false;

			var state = game.ApplyState(allTurns.SelectMany(x => x.StateChanges));

			if(state is null)
				return false;

			try
			{
				state.ApplyAction(proposedAction);
				return state != null;
			}
			catch
			{
				return false;
			}
		}
		
		public async Task<List<Model.CoinType>> GetAvailableCards(int id)
		{
			var game = await GameRepository.GetGame(id);

			if (game is null)
				return null;

			var cardsUsed = game.Players.SelectMany(x => x.Coins);
			return AllCoins.Except(cardsUsed).ToList();
		}

		private ImmutableList<Model.CoinType> AvailableCardsFromGame(Model.Game game) => AllCoins.RemoveRange(game.Players.SelectMany(x => x.Coins));

		public static ImmutableList<Model.CoinType> AllCoins = ImmutableList<Model.CoinType>.Empty.AddRange(Enum.GetValues(typeof(Model.CoinType)).Cast<Model.CoinType>());
	}
}
