﻿using System.Threading.Tasks;
using AutoMapper;

namespace Api.Facades
{
	using Services;
	using Repositories.Player;

	public class PlayerFacade
	{
		private readonly IPlayerRepository Repository;
		private readonly IMapper Mapper;
		private readonly IUserInfoProvider userInfoProvider;

		public PlayerFacade(IPlayerRepository repository, IMapper mapper, IUserInfoProvider userInfoProvider)
		{
			Repository = repository;
			Mapper = mapper;
			this.userInfoProvider = userInfoProvider;
		}

		public async Task<dto.Player> Me()
		{
			// get userId from token.
			var pid = userInfoProvider.GetUserId();
			if (pid is null)
				return null;
			var history = await Repository.GetPlayer(userInfoProvider.GetUserId().GetValueOrDefault());
			return Mapper.Map<dto.Player>(history);
		}

		public async Task<dto.Player> Register(int playerId)
		{
			// create the user.
			// var playerId = userInfoProvider.GetUserId(); // TODO: once we have oauth hooked in on the front end, we could get the playerid from the token.
			var player = new Model.Player(playerId, "");
			var playerModel = await Repository.RegisterPlayer(player);
			return Mapper.Map<dto.Player>(playerModel);
		}


	}
}
