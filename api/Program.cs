﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Api
{
	public class Program
	{
		public static async Task Main(string[] args)
		{
			var config = BuildConfig(args).Build();
			var builder = CreateWebHostBuilder(args, config);
			var host = builder.Build();

			await SetupDatabase(host);

			await host.RunAsync();
			System.Console.WriteLine("exiting");
		}

		public static IHostBuilder CreateHostBuilder(string[] args)
			=> Host.CreateDefaultBuilder(args)
			.ConfigureWebHostDefaults(
					webBuilder => webBuilder.UseStartup<Startup>());

		public static async Task SetupDatabase(IWebHost host)
		{
			using (var serviceScope = host.Services.GetService<IServiceScopeFactory>().CreateScope())
			{
				var context = serviceScope.ServiceProvider.GetRequiredService<db.DataContext>();
				await context.Database.MigrateAsync();
			}
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args, IConfiguration config)
		{
			var hostUrl = config["url"];
			if (int.TryParse(config["port"], out var port))
				hostUrl += ":" + port;
			return WebHost.CreateDefaultBuilder(args)
				.UseUrls(hostUrl)
				.UseConfiguration(config)
				.UseStartup<Startup>();
		}

		public static IConfigurationBuilder BuildConfig(string[] args, IConfigurationBuilder builder = null)
		{
			if (builder is null)
				builder = new ConfigurationBuilder();
			return builder
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddEnvironmentVariables()
				.AddCommandLine(args);
		}
	}
}
