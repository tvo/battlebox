﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;

namespace Api
{
	public class Map : Profile
	{
		public Map()
		{
			// game
			//CreateMap<dto.Game, Model.Game>()
			//	.ConstructUsing(g => new Model.Game(0, g.Name, g.Players.Select(x => new Model.Gamer(x.Id, g.Id, (Model.Team)(x.Id % 2), x., 0)).ToList(), g.CreatedDate, g.CreatedById))
			//	.IgnoreAllPropertiesWithAnInaccessibleSetter()
			//	;
			CreateMap<Model.Game, db.Game>()
				;


			CreateMap<db.Game, Model.Game>()
				.ConstructUsing((g, resolve) => new Model.Game(g.Id, g.Name, resolve.Mapper.Map<List<Model.Gamer>>(g.Players), g.CreatedTimeUtc, g.CreatedById))
				.ForMember(x => x.Players, opt => opt.Ignore())
				.IgnoreAllPropertiesWithAnInaccessibleSetter()
				;

			CreateMap<Model.Game, dto.Game>()
				.ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
				.ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate))
				.ForMember(x => x.GameType, opt => opt.MapFrom(x => x.Players.Count == 4 ? Model.GameType.FourPlayer : Model.GameType.TwoPlayer))
				;

			// player
			CreateMap<db.Player, Model.Player>()
				.ConstructUsing(p => new Model.Player(p.Id, p.Id.ToString()))
				.ReverseMap();
			CreateMap<dto.Player, Model.Player>()
				.ConstructUsing(p => new Model.Player(p.Id, p.Id.ToString()))
				.ReverseMap();

			// gamer
			CreateMap<db.PlayerInGame, Model.Gamer>()
				.ConstructUsing(g => new Model.Gamer(g.PlayerId, g.GameId, (Model.Team)g.Team, g.GetCards().Cast<Model.CoinType>(), 0));
			CreateMap<Model.Gamer, db.PlayerInGame>();


			CreateMap<Model.Gamer, dto.Player>()
				.ForMember(x => x.Id, opt => opt.MapFrom(x => x.PlayerId))
				.ForMember(x => x.Name, opt => opt.MapFrom(x => x.PlayerId.ToString()))
				;

			// Lobby
			CreateMap<db.Lobby, Model.Lobby>()
				.ConstructUsing(x => new Model.Lobby(
							x.Id,
							x.Name,
							x.GameType,
							x.LobbyPlayers.Where(x => x.Team == 0).Select(x => x.PlayerId),
							x.LobbyPlayers.Where(x => x.Team == 1).Select(x => x.PlayerId),
							x.CreatedDate,
							x.CreatedById));
			CreateMap<Model.Lobby, db.Lobby>()
				.ForMember(x => x.GameType, opt => opt.MapFrom(x => x.RequiredPlayers == 2 ? Model.GameType.TwoPlayer : Model.GameType.FourPlayer))
				.ForMember(x => x.LobbyPlayers, opt => opt.Ignore())
				;
			CreateMap<Model.Lobby, dto.Lobby>()
				.ForMember(x => x.GameType, opt => opt.MapFrom(x => x.RequiredPlayers == 2 ? Model.GameType.TwoPlayer : Model.GameType.FourPlayer))
				.ForMember(x => x.TeamA, opt => opt.Ignore())
				.ForMember(x => x.TeamB, opt => opt.Ignore())
				;
			CreateMap<dto.LobbyCreate, Model.Lobby>()
				.ConstructUsing(x => new Model.Lobby(0, x.Name, x.GameType, x.TeamA, x.TeamB, x.CreatedDate, x.CreatedById));

			// coin positions
			CreateMap<Model.Positioning.BoardPosition<Model.Positioning.Cube>, dto.HexPosition>().IncludeAllDerived()
				.ForMember(x => x.X, opt => opt.MapFrom(src => src.Position.X))
				.ForMember(x => x.Y, opt => opt.MapFrom(src => src.Position.Y))
				.ForMember(x => x.Z, opt => opt.MapFrom(src => src.Position.Z))
				;
			CreateMap<dto.HexPosition, Model.Positioning.BoardPosition<Model.Positioning.Cube>>()
				.ConstructUsing(d => new Model.Positioning.BoardPosition<Model.Positioning.Cube>(new Model.Positioning.Cube(d.X, d.Y, d.Z), d.Control));

			CreateMap<dto.Position, Model.Positioning.IPosition>().As<Model.Positioning.Cube>();
			CreateMap<dto.Position, Model.Positioning.Cube>().ConstructUsing(dto => new Model.Positioning.Cube(dto.X, dto.Y, dto.Z));
			CreateMap<Model.Positioning.IPosition, dto.Position>().IncludeAllDerived();
			CreateMap<Model.Positioning.Cube, dto.Position>();
		}
	}
}
