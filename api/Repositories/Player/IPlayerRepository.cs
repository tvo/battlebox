﻿using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Collections.Generic;

namespace Api.Repositories.Player
{
	using Model;

	public interface IPlayerRepository
	{
		Task<Player> GetPlayer(int id);
		Task<ImmutableDictionary<int, Model.Player>> GetPlayers(IEnumerable<int> playerIds);
		Task<Player> RegisterPlayer(Player player);
	}
}
