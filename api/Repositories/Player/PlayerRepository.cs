﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Api.Repositories.Player
{
	public class PlayerRepository : IPlayerRepository
	{
		private readonly db.DataContext Context;
		private readonly IMapper Mapper;

		public PlayerRepository(db.DataContext context, IMapper mapper)
		{
			Context = context;
			Mapper = mapper;
		}

		public async Task<Model.Player> GetPlayer(int id)
		{
			var player = await Context.Players
				.AsNoTracking()
				.FirstOrDefaultAsync(x => x.Id == id);

			if(player is null)
				return null;
			return Mapper.Map<Model.Player>(player);
		}

		public async Task<ImmutableDictionary<int, Model.Player>> GetPlayers(IEnumerable<int> playerIds)
		{
			var ids = playerIds.ToList();
			var players= await Context.Players
				.AsNoTracking()
				.Where(x => ids.Contains(x.Id))
				.ToListAsync();
			if(players is null)
				return null;
			return ImmutableDictionary<int, Model.Player>.Empty.AddRange(
					Mapper.Map<List<Model.Player>>(players).Select(x => new KeyValuePair<int, Model.Player>(x.Id, x))
					)
					;
		}

		public async Task<Model.Player> RegisterPlayer(Model.Player player)
		{
			var dbPlayer = Mapper.Map<db.Player>(player);
			dbPlayer.Id = ((await Context.Players.OrderByDescending(x => x.Id).FirstOrDefaultAsync())?.Id ?? 0) + 1;


			Context.Add(dbPlayer);
			await Context.SaveChangesAsync();
			player = Mapper.Map<Model.Player>(dbPlayer);
			return player;
		}
	}

}
