﻿using System.Threading.Tasks;
using System.Collections.Immutable;

namespace Api.Repositories.Lobby
{
	public interface ILobbyRepository
	{
		// Task<Lobby> GetLobbyForPlayer(int playerId);
		Task<ImmutableList<int>> GetListOfPlayerIdsForLobby(int lobbyId); // delete ?
		// Task<ImmutableList<Model.Lobby>> GetListOfPlayersForLobby(int lobbyId); // delete?
		Task<ImmutableList<Model.Lobby>> GetLobbies();
		Task<Model.Lobby> GetLobby(int lobbyId);
		Task<Model.Lobby> Create(Model.Lobby lobby);
		Task<bool> AddPlayerToLobby(int lobbyId, int playerId, Model.Team team);
		Task<bool> RemovePlayerToLobby(int lobbyId, int playerId);
		Task<bool> Delete(int lobbyId);
	}
}
