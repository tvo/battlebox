﻿using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Api.Repositories.Lobby 
{
	public class LobbyRepository : ILobbyRepository
	{
		private readonly db.DataContext Context;
		private readonly IMapper Mapper;

		public LobbyRepository(db.DataContext context, IMapper mapper)
		{
			Context = context;
			Mapper = mapper;
		}

		public async Task<bool> IsPlayerInLobby (int lobbyId, int playerId)
		{
			return await Context.PlayerInLobbies.AnyAsync(x => x.LobbyId == lobbyId && x.PlayerId == playerId);
		}

		public async Task<ImmutableList<Model.Lobby>> GetLobbies()
		{
			var lobbies = await Context.Lobby.Include(x => x.LobbyPlayers).ToListAsync();
			var m = Mapper.Map<List<Model.Lobby>>(lobbies);
			return ImmutableList<Model.Lobby>.Empty.AddRange(m);
		}

		public async Task<Model.Lobby> GetLobby(int lobbyId)
		{
			var lobbies = await Context.Lobby.Include(x => x.LobbyPlayers).FirstOrDefaultAsync(x => x.Id == lobbyId);
			return Mapper.Map<Model.Lobby>(lobbies);
		}

		public async Task<Model.Lobby> Get(int lobbyId)
		{
			var lobby = await Context.Lobby.Include(x => x.LobbyPlayers).FirstOrDefaultAsync(x => x.Id == lobbyId);
			return Mapper.Map<Model.Lobby>(lobby);
		}

		public async Task<ImmutableList<int>> GetListOfPlayerIdsForLobby (int lobbyId)
		{
			return ImmutableList<int>.Empty.AddRange(await Context.Lobby.Where(x => x.Id == lobbyId).Select(x => x.Id).ToListAsync());
		}

		public async Task<ImmutableList<Model.Lobby>> GetListOfPlayersForLobby (int lobbyId)
		{
			return ImmutableList<Model.Lobby>.Empty.AddRange(Mapper.Map<List<Model.Lobby>>(await Context.Lobby.Where(x => x.Id == lobbyId).ToListAsync()));
		}
	
		public async Task<bool> AddPlayerToLobby (int lobbyId, int playerId, Model.Team team)
		{
			// check stuff exists first.
			if(!await Context.Lobby.AnyAsync(x => x.Id == lobbyId))
				return false;
			if(!await Context.Players.AnyAsync(x => x.Id == playerId))
				return false;
			if(await IsPlayerInLobby(lobbyId, playerId))
				return false;

			// go ahead and add it.
			Context.PlayerInLobbies.Add(new db.PlayerInLobby{PlayerId = playerId, LobbyId = lobbyId, Team = (int)team});
			await Context.SaveChangesAsync();
			return true;
		}

		public async Task<Model.Lobby> Create (Model.Lobby model)
		{
			var entity = Mapper.Map<db.Lobby>(model);
			entity.LobbyPlayers = entity.LobbyPlayers ?? new List<db.PlayerInLobby>();
			entity.Id = ((await Context.Lobby.OrderByDescending(x => x.Id).FirstOrDefaultAsync())?.Id ?? 0) + 1;
			foreach (var aId in model.TeamAPlayerIds)
			{
				entity.LobbyPlayers.Add(new db.PlayerInLobby{
					LobbyId = entity.Id,
					PlayerId = aId,
					Team = 0
				});
			}
			foreach (var bId in model.TeamBPlayerIds)
			{
				entity.LobbyPlayers.Add(new db.PlayerInLobby{
					LobbyId = entity.Id,
					PlayerId = bId,
					Team = 1
				});
			}
			Context.Add(entity);
			await Context.SaveChangesAsync();
			model = Mapper.Map<Model.Lobby>(entity);
			return model;
		}

		public async Task<bool> RemovePlayerToLobby (int lobbyId, int playerId)
		{
			// check stuff exists first.
			var entity = await Context.PlayerInLobbies.FirstOrDefaultAsync(x => x.LobbyId == lobbyId && x.PlayerId == playerId);
			if(entity is null)
				return false;
			Context.PlayerInLobbies.Remove(entity);
			await Context.SaveChangesAsync();
			return true;
		}

		public async Task<ImmutableList<int>> PlayersForLobby (int lobbyId)
		{
			return ImmutableList<int>.Empty.AddRange(await Context.PlayerInLobbies.Where(x => x.LobbyId == lobbyId).Select(x => x.PlayerId).ToListAsync());
		}

		public async Task<bool> Delete(int lobbyId)
		{
			var entity = await Context.Lobby.FirstOrDefaultAsync(x => x.Id == lobbyId);
			if (entity is null)
				return false;

			var playerInLobby = await Context.PlayerInLobbies.Where(x => x.LobbyId == lobbyId).ToListAsync();
			Context.RemoveRange(playerInLobby);
			Context.Remove(entity); 

			return (await Context.SaveChangesAsync()) == (1 + playerInLobby.Count);
		}

	}
}
