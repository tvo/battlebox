﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Api.Repositories.Game
{
	public class GameRepository : IGameRepository
	{
		private readonly db.DataContext Context;
		private readonly IMapper Mapper;

		public GameRepository(db.DataContext context, IMapper mapper)
		{
			Context = context;
			Mapper = mapper;
		}

		public async Task<List<Model.Game>> GetGames()
		{
			var query = await Context.Games
				.Include(x => x.Players)
				.AsNoTracking().ToListAsync();
			return Mapper.Map<List<Model.Game>>(query);
		}

		public async Task<Model.Game> GetGame(int gameId)
		{
			var game = await Context.Games
				.Include(x => x.Players)
				.AsNoTracking().FirstOrDefaultAsync(x => x.Id == gameId);

			if(game is null)
				return null;
			return Mapper.Map<Model.Game>(game);
		}

		public async Task<bool> Delete(int id)
		{
			var game = await Context.Games.Include(x => x.Players).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
			if(game is null)
				return false;
			Context.Games.Remove(game);
			await Context.SaveChangesAsync();
			return true;
		}

		public async Task<Model.Game> Create(Model.Game game)
		{
			// only two or four players can play a game.
			if (!game.ValidPlayers())
				return null;

			var dbGame = Mapper.Map(game, new db.Game());

			// set the server side values.
			dbGame.CreatedTimeUtc = DateTime.UtcNow;
			dbGame.Id = ((await Context.Games.AsNoTracking().OrderByDescending(x => x.Id).FirstOrDefaultAsync())?.Id ?? 0) + 1;

			var gamers = new List<Model.Gamer>();
			foreach(var gmr in game.Players)
			{
				gamers.Add(new Model.Gamer(gmr.PlayerId, dbGame.Id, gmr.Team, null, gmr.Order));
			}

			Context.Games.Add(dbGame);
			await Context.SaveChangesAsync();
			return Mapper.Map<Model.Game>(dbGame);
		}

		public async Task<bool> DrawCard(Model.Game game, int playerId, Model.CoinType coinType)
		{
			var player = game.GetPlayer(playerId);

			// update db
			var dbPlayer = await Context.Gamers.FirstAsync(x => x.PlayerId == player.PlayerId && x.GameId == game.Id);
			dbPlayer.DrawCard((int)coinType);
			await Context.SaveChangesAsync();

			return true;
		}
	}
}
