﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace Api.Repositories.Game
{
	using Model;

	public interface IGameRepository
	{
		Task<Game> GetGame(int gameId);
		Task<bool> Delete(int gameId);
		Task<List<Game>> GetGames();
		Task<Game> Create(Game game);
		Task<bool> DrawCard(Model.Game gameId, int playerId, Model.CoinType coinType);
	}
}
