﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace Api.Repositories.Action
{
	using Model;
	using dto.Responses;

	public interface ITurnRepository
	{
		Task<List<Turn>> GameHistory(int gameId);
		Task<Turn> GetMostRecentTurn(int gameId);
		Task<FacadeResponse> SaveTurn(int gameId, Model.Turn turn);
	}
}
