﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Api.dto.Responses;

namespace Api.Repositories.Action
{
	public class TurnRepository : ITurnRepository
	{
		private readonly db.DataContext Context;
		private readonly IMapper Mapper;

		public TurnRepository(db.DataContext context, IMapper mapper)
		{
			Context = context;
			Mapper = mapper;
		}

		public async Task<List<Model.Turn>> GameHistory(int gameId)
		{
			var stateChanges = await Context.StateChanges
				.AsNoTracking()
				.Include(x => x.Game)
				.ThenInclude(x => x.Players)
				.Where(x => x.GameId == gameId)
				.OrderBy(x => x.Time)
				.ToListAsync();

			var grouped = stateChanges.GroupBy(x => new { x.TurnNumber, x.RoundNumber });

			var turns = new List<Model.Turn>();

			foreach(var group in grouped)
			{
				turns.Add(ConvertStateChangesToTurn(group));
			}

			return turns;
		}

		public async Task<Model.Turn> GetMostRecentTurn(int gameId) => ConvertStateChangesToTurn(await FetchRecentTurn(gameId));

		public async Task<FacadeResponse> SaveTurn(int gameId, Model.Turn turn)
		{
			var oldChanges = await FetchRecentTurn(gameId);
			var mostRecent = ConvertStateChangesToTurn(oldChanges);

			db.StateChange CreateChangeFromTurn(Model.Actions.Action change, int changeNumber)
			{
				var dbChange = new db.StateChange();
				dbChange.RoundNumber = turn.RoundNumber;
				dbChange.TurnNumber = turn.TurnNumber;
				dbChange.ChangeNumber = changeNumber;
				dbChange.Time = change.Time;
				dbChange.PlayerId = turn.PlayerId;
				dbChange.GameId = gameId;
				dbChange.Change = change.Serialize();
				return dbChange;
			}

			// determine if this is a new turn
			if(mostRecent is null || !mostRecent.SameTurnAs(turn))
			{
				for(var i = 0; i < turn.StateChanges.Count; ++i)
				{
					var change = turn.StateChanges[i];
					Context.StateChanges.Add(CreateChangeFromTurn(change, i));
				}
			}
			// not a new turn, need to update then
			else
			{
				for(var i = 0; i < turn.StateChanges.Count; ++i)
				{
					var change = turn.StateChanges[i];
					if(mostRecent.StateChanges.Contains(change))
						continue;
					Context.StateChanges.Add(CreateChangeFromTurn(change, i));
				}
			}

			await Context.SaveChangesAsync();

			return NoContentResponse.Instance;
		}
	

		private async Task<List<db.StateChange>> FetchRecentTurn(int gameId)
		{
			// get max turn number.
			var mostRecentTurn = (Context.StateChanges.AsNoTracking()
				.Where(x => x.GameId == gameId)
				.OrderBy(x => x.RoundNumber).ThenBy(x => x.TurnNumber).ThenBy(x => x.Time)
				.LastOrDefault());
			if(mostRecentTurn is null)
				return new List<db.StateChange>();

			var changesPerTurn = await Context.StateChanges
				.AsNoTracking()
				.Include(x => x.Game)
				.ThenInclude(x => x.Players)
				.Where(x => x.GameId == gameId && x.RoundNumber == mostRecentTurn.RoundNumber)
				.OrderBy(x => x.Time)
				.ToListAsync();
			return changesPerTurn;
		}

		private Model.Turn ConvertStateChangesToTurn(IEnumerable<db.StateChange> changesPerTurn)
		{
			var f = changesPerTurn?.FirstOrDefault();
			if (f is null)
				return null;
			var game = Mapper.Map<Model.Game>(f.Game);
			return ConvertStateChangesToTurn(changesPerTurn, game);
		}


		private Model.Turn ConvertStateChangesToTurn(IEnumerable<db.StateChange> changesPerTurn, Model.Game game)
		{
			var f = changesPerTurn?.FirstOrDefault();
			if (changesPerTurn is null)
				return null;

			var result = new Model.Turn(f.PlayerId, f.RoundNumber, f.TurnNumber, changesPerTurn.Select(x => Model.Actions.Action.Parse(x.Change, game.GetPlayer(x.PlayerId), x.Time)));

			return result;
		}

	}
}
