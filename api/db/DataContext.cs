﻿using Microsoft.EntityFrameworkCore;

namespace Api.db
{
	public class DataContext : DbContext
	{
		public DataContext (DbContextOptions<DataContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<Game>().HasMany(x => x.Players);
			builder.Entity<PlayerInGame>().HasKey(x => new { x.PlayerId, x.GameId });
			builder.Entity<PlayerInGame>().HasOne(x => x.Player);
			builder.Entity<PlayerInGame>().HasOne(x => x.Game);
			builder.Entity<Player>().HasKey(x => x.Id);
			builder.Entity<StateChange>().HasKey(x => new { x.GameId, x.ChangeNumber, x.TurnNumber, x.RoundNumber });
			builder.Entity<PlayerInLobby>().HasKey(x => new { x.PlayerId, x.LobbyId });
		}

		public DbSet<Game> Games { get; set; }
		public DbSet<PlayerInGame> Gamers { get; set; }
		public DbSet<Player> Players { get; set; }
		public DbSet<StateChange> StateChanges { get; set; }
		public DbSet<Lobby> Lobby { get; set; }
		public DbSet<PlayerInLobby> PlayerInLobbies { get; set; }
	}
}
