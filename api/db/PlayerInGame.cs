﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api.db
{
	public class PlayerInGame
	{
		[Key]
		public int PlayerId { get; set; }
		[Key]
		public int GameId { get; set; }
		public int Team { get; set; }
		public int CardOne { get; set; } = -1;
		public int CardTwo { get; set; } = -1;
		public int CardThree { get; set; } = -1;
		public int CardFour { get; set; } = -1;

		public IEnumerable<int> GetCards() => new []{CardOne, CardTwo, CardThree, CardFour}.Where(x => x >= 0);
		public void DrawCard(int card)
		{
			if(CardOne == -1)
				CardOne = card;
			else if (CardTwo == -1)
				CardTwo = card;
			else if (CardThree == -1)
				CardThree = card;
			else if (CardFour == -1)
				CardFour = card;
		}

		public Player Player { get; set; }
		public Game Game { get; set; }
	}
}
