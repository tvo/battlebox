﻿namespace Api.db
{
	public class PlayerInLobby
	{
		public int LobbyId { get; set; }
		public int PlayerId { get; set; }
		public int Team { get; set; }
	}
}
