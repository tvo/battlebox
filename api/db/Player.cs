﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.db
{
	public class Player
	{
		[Key]
		public int Id { get; set; }
		public DateTime Time { get; set; }
		public string Sub { get; set; }
		public string PictureUrl { get; set; }
		public string Name { get; set; }
		public string Nickname { get; set; }
		public string Locale { get; set; }
	}
}
