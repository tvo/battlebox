﻿using System;

namespace Api.db
{
	public class StateChange : IEquatable<StateChange>
	{
		public int GameId { get; set; }
		public int RoundNumber { get; set; } // Increments when all players do their thing.
		public int TurnNumber { get; set; } // Each player has one per round.
		public int ChangeNumber { get; set; } // An action might trigger many state changes.
		public int PlayerId { get; set; } // not always who's turn it is.
		public DateTime Time { get; set; } // utc
		public string Change { get; set; } // string serialized change content.

		public virtual Game Game { get; set; } // set by ef core.

		public bool Equals(StateChange other)
		{
			return GameId.Equals(other.GameId) &&
				RoundNumber.Equals(other.RoundNumber) &&
				TurnNumber.Equals(other.TurnNumber) &&
				ChangeNumber.Equals(other.ChangeNumber) &&
				PlayerId.Equals(other.PlayerId) &&
				Time.Equals(other.Time) &&
				Change.Equals(other.Change);
		}

		public override int GetHashCode()
		{
			return GameId.GetHashCode() ^
				RoundNumber.GetHashCode() ^
				TurnNumber.GetHashCode() ^
				ChangeNumber.GetHashCode() ^
				PlayerId.GetHashCode() ^
				Time.GetHashCode() ^
				Change.GetHashCode();
		}
		public override bool Equals(object other)
		{
			if(other is StateChange sc)
				return Equals(other);
			return false;
		}

	}
}
