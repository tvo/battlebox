﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;
namespace Api.db
{
	public class Lobby
	{
		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public Model.GameType GameType { get; set; }
		public DateTime CreatedDate { get; set; }
		public int CreatedById { get; set; }
		public List<PlayerInLobby> LobbyPlayers { get; set; }
	}
}
