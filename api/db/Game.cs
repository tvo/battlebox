﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api.db
{
	public class Game
	{
		[Key]
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime CreatedTimeUtc { get; set; }
		public int CreatedById { get; set; }
		public List<PlayerInGame> Players { get; set; }
		public int PlayersTurn { get; set; }
		public int Phase { get; set; }
		public string PlayerOrder { get; set; }
	}
}

