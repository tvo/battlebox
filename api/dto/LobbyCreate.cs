﻿using System;
using System.Collections.Generic;

namespace Api.dto
{
	public class LobbyCreate
	{
		public string Name { get; set; }
		public Model.GameType GameType { get; set; }
		public List<int> TeamA { get; set; }
		public List<int> TeamB { get; set; }
		public DateTime CreatedDate { get; set; }
		public int CreatedById { get; set; }
	}
}
