﻿namespace Api.dto
{
	public class DeletedDto
	{
		public int Id { get; set; }
	}
}
