﻿namespace Api.dto.Responses
{
	public abstract class FacadeResponse
	{
		public bool IsSuccess { get; protected set; }
	}
}
