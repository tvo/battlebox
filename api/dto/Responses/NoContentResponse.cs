﻿namespace Api.dto.Responses
{
	public class NoContentResponse : FacadeResponse
	{
		private NoContentResponse()
		{
			IsSuccess = true;
		}

		public static FacadeResponse Instance = new NoContentResponse();
	}
}
