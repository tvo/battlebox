﻿namespace Api.dto.Responses
{
	public interface IHaveContent
	{
		public object GetContent();
	}
}
