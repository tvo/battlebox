﻿namespace Api.dto.Responses
{
	public class ErrorResponse : FacadeResponse
	{
		public int? ErrorCode { get; } = null;
		public string Description { get; }

		public ErrorResponse(string description, int? errorCode = null)
		{
			Description = description;
			ErrorCode = errorCode;
			IsSuccess = false;
		}
	}
}
