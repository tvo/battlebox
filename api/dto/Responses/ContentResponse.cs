﻿namespace Api.dto.Responses
{
	public class ContentResponse<T> : FacadeResponse, IHaveContent
	{
		public readonly T Content;

		public ContentResponse(T content)
		{
			this.Content = content;
			IsSuccess = true;
		}

		public object GetContent() => Content;
	}

	public static class ContentResponse
	{
		public static ContentResponse<T> From<T>(T content) => new ContentResponse<T>(content);
	}
}
