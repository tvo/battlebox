﻿using System;
using System.Collections.Generic;


namespace Api.dto
{
	public class Lobby
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public Model.GameType GameType { get; set; }
		public List<Player> TeamA { get; set; }
		public List<Player> TeamB { get; set; }
		public DateTime CreatedDate { get; set; }
		public int CreatedById { get; set; }
	}
}
