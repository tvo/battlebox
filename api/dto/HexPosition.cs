﻿namespace Api.dto
{
	public class HexPosition
	{
		public int X { get; set; }
		public int Y { get; set; }
		public int Z { get; set; }
		public bool Control { get; set; }
	}
}
