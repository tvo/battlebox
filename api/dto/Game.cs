﻿using System;
using System.Collections.Generic;
using Api.Model;

namespace Api.dto
{
	public class Game
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public GameType GameType { get; set; }
		public DateTime CreatedDate { get; set; }
		public int CreatedById { get; set; }
		public List<Player> Players { get; set; } = new List<Player>();
		public int PlayersTurnId { get; set; }
	}
}
