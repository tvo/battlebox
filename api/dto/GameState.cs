﻿using System.Collections.Generic;
using Api.Model;

namespace Api.dto
{
	public class PlayOrder
	{
		public int PlayerId { get; set; }
		public bool IsTurn { get; set; }
		public bool IsRoundStarter { get; set; }
	}

	public enum Team
	{
		Unknown = 0,
		Crow = 1,
		Coyote = 1,
	}

	public class ControlledPoints : Position
	{
		public Team Team { get; set; }
	}

	public class BoardCoin : Position
	{
		public int PlayerId { get; set; }
	}

	public enum CoinType
	{
		FaceDown = 0,
		Archer = 1,
		Bannerman = 2,
		Berserker = 3,
		Bishop = 4,
		Cavalry = 5,
		Crossbowman = 6,
		Earl = 7,
		Ensign = 8,
		Footman = 9,
		Herald = 10,
		Knight = 11,
		Lancer = 12,
		LightCavalry = 13,
		Marshall = 14,
		Mercenary = 15,
		Pikeman = 16,
		RoyalGuard = 17,
		Scout = 18,
		Swordsman = 19,
		WarriorPriest = 20
	}

	public enum Location
	{
		Exile = 0,
		Bag = 1,
		Hand = 2,
		DiscardUp = 3,
		DiscardDown = 4
	}

	public class OtherCoin
	{
		public int PlayerId { get; set; }
		public CoinType CoinType { get; set; }
		public int Count { get; set; }
		public int Location { get; set; } // this will be converted to an enum eventually.
	}

	public class Decree
	{
		public DecreeEnum DecreeType { get; set; }
		public Team Team;
	}

	public class GameState
	{
		public List<PlayOrder> PlayOrder { get; set; }
		public List<ControlledPoints> ControlPoints { get; set; }
		public List<BoardCoin> BoardCoins { get; set; }
		public List<OtherCoin> OtherCoins { get; set; }
		public List<Decree> Decrees { get; set; }
		public List<DecreeEnum> Available { get; set; }
	}
}
