﻿namespace Api.dto
{
	public class TransferDto
	{
		public Model.CoinLocation From { get; set; }
		public Model.CoinLocation To { get; set; }
		public Model.CoinType Type { get; set; }
		public int Amount { get; set; }
	}
}
