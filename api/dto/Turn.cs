﻿namespace Api.dto
{
	public class Turn
	{
		public string Action { get; set; }
		public Model.Actions.Action Change { get; set; }
		public int PlayerId { get; set; }
	}
}
