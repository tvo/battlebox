﻿namespace Api.dto
{
	public class RemoveDto
	{
		public Position From { get; set; }
		public Model.CoinLocation To { get; set; }
	}
}
