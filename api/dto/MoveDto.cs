﻿namespace Api.dto
{
	public class MoveDto
	{
		public Position From { get; set; }
		public Position To { get; set; }
	}
}
