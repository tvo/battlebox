﻿namespace Api.dto
{
	public class DeployDto
	{
		public Model.CoinType Card { get; set; }
		public Model.CoinLocation Location { get; set; }
		public Position To { get; set; }
	}
}
