﻿using System;

namespace Api.Model.Actions
{
	using Positioning;

	public class Control : Action<Control>
	{
		public readonly Cube Position;

		public Control(IPosition controlLocation, Gamer player, DateTime time) : base(player, time)
		{
			Position = Cube.ConverToCube(controlLocation);
		}

		public override string Serialize() => IdentifierString + $"{Position}";

		public static Control Deserialize(string content, Gamer player, DateTime time)
		{
			if(!CanDeserialize(content))
				return default;
			content = content.Substring(IdentifierString.Length);

			var from = Cube.Parse(content);

			return new Control(from, player, time);
		}
	}
}
