﻿using System;
using System.Collections.Generic;

namespace Api.Model.Actions
{
	public class PickCard : Action<PickCard>
	{
		public readonly CoinType CoinType;

		public PickCard(CoinType coin, Gamer player, DateTime time) : base(player, time)
		{
			CoinType = coin;
		}

		public override string Serialize() => IdentifierString + CoinType;

		public static PickCard Deserialize(string content, Gamer player, DateTime time)
		{
			if(!CanDeserialize(content))
				return default;
			content = content.Split('|', 2)[1];
			if(Enum.TryParse<CoinType>(content, out var coinType))
				return new PickCard(coinType, player, time);

			return default;
		}

		public static readonly Dictionary<CoinType, int> StartingAmount = new Dictionary<CoinType, int>()
		{
			{CoinType.Archer, 4},
			{CoinType.Bannerman, 4},
			{CoinType.Berserker, 5},
			{CoinType.Bishop, 5},
			{CoinType.Cavalry, 4},
			{CoinType.Crossbowman, 5},
			{CoinType.Earl, 5},
			{CoinType.Ensign, 5},
			{CoinType.FaceDown, 5},
			{CoinType.Footman, 5},
			{CoinType.Herald, 5},
			{CoinType.Knight, 4},
			{CoinType.Lancer, 4},
			{CoinType.LightCavalry, 5},
			{CoinType.Marshall, 5},
			{CoinType.Mercenary, 5},
			{CoinType.Pikeman, 4},
			{CoinType.RoyalGuard, 5},
			{CoinType.Scout, 5},
			{CoinType.Swordsman, 5},
			{CoinType.WarriorPriest, 4}
		};
	}
}
