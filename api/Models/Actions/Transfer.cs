﻿using System;

namespace Api.Model.Actions
{
	public class Transfer : Action<Transfer>
	{
		public readonly CoinLocation From;
		public readonly CoinLocation To;
		public readonly CoinType Type;
		public readonly int Amount;

		public Transfer(CoinLocation from, CoinLocation to, CoinType type, int amount, Gamer player, DateTime time) : base(player, time)
		{
			From = from;
			To = to;
			Type = type;
			Amount = amount;
		}

		public override string Serialize() => IdentifierString + $"{(int)From}/{(int)To}/{(int)Type}/{Amount}";

		public static Transfer Deserialize(string content, Gamer player, DateTime time)
		{
			if(!CanDeserialize(content))
				return default;
			content = content.Substring(IdentifierString.Length);
			var split = content.Split('/', 4);

			var from = (CoinLocation)int.Parse(split[0]);
			var to = (CoinLocation)int.Parse(split[1]);
			var type = (CoinType)int.Parse(split[2]);
			var amount = int.Parse(split[3]);

			return new Transfer(from, to, type, amount, player, time);
		}
	}
}
