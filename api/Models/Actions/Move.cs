﻿using System;

namespace Api.Model.Actions
{
	using Positioning;

	public class MoveInBoard : Action<MoveInBoard>
	{
		public readonly Cube From;
		public readonly Cube To;

		public MoveInBoard(IPosition from, IPosition to, Gamer player, DateTime time) : base(player, time)
		{
			From = Cube.ConverToCube(from);
			To = Cube.ConverToCube(to);
		}

		public override string Serialize() => IdentifierString + $"{From}/{To}";

		public static MoveInBoard Deserialize(string content, Gamer player, DateTime time)
		{
			if(!CanDeserialize(content))
				return default;
			content = content.Substring(IdentifierString.Length);
			var split = content.Split('/', 2);

			var from = Cube.Parse(split[0]);
			var to = Cube.Parse(split[1]);

			return new MoveInBoard(from, to, player, time);
		}
	}
}
