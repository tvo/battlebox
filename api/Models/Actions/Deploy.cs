﻿using System;

namespace Api.Model.Actions
{
	using Positioning;

	public class Deploy : Action<Deploy>
	{
		public readonly Cube To;
		public readonly CoinType CoinUsed;
		public readonly CoinLocation Location;

		public Deploy(IPosition to, CoinLocation location, CoinType used, Gamer player, DateTime time) : base(player, time)
		{
			if(to is null) throw new ArgumentNullException(nameof(to));
			To = Cube.ConverToCube(to);
			CoinUsed = used;
			Location = location;
		}

		public override string Serialize() => IdentifierString + $"{To}/{(int)CoinUsed}/{Location}";

		public static Deploy Deserialize(string content, Gamer player, DateTime time)
		{
			if(!CanDeserialize(content))
				return default;
			content = content.Substring(IdentifierString.Length);
			var split = content.Split("/", 3);

			var to = Cube.Parse(split[0]);
			var used = Enum.Parse<CoinType>(split[1]);
			var location = Enum.Parse<CoinLocation>(split[2]);

			return new Deploy(to, location, used, player, time);
		}
	}
}
