﻿using System;

namespace Api.Model.Actions
{
	using Positioning;

	public class Remove : Action<Remove>
	{
		public readonly Cube From;
		public readonly CoinLocation To;

		public Remove(IPosition from, CoinLocation to, Gamer player, DateTime time) : base(player, time)
		{
			From = Cube.ConverToCube(from);
			To = to;
		}

		public override string Serialize() => IdentifierString + $"{From}/{(int)To}";

		public static Remove Deserialize(string content, Gamer player, DateTime time)
		{
			if(!CanDeserialize(content))
				return default;
			content = content.Substring(IdentifierString.Length);
			var split = content.Split('/', 2);

			var from = Cube.Parse(split[0]);
			var to = (CoinLocation)int.Parse(split[1]);

			return new Remove(from, to, player, time);
		}
	}
}
