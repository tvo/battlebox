﻿using System;

namespace Api.Model.Actions
{
	public abstract class Action : IEquatable<Action>
	{
		public Gamer Player { get; }
		public DateTime Time { get; }

		protected Action(Gamer player, DateTime time)
		{
			Player = player;
			Time = time;
		}

		public abstract string Serialize();

		public static Action Parse(string content, Gamer player, DateTime time)
		{
			if(Transfer.CanDeserialize(content))
				return Transfer.Deserialize(content, player, time);
			if(Remove.CanDeserialize(content))
				return Remove.Deserialize(content, player, time);
			if(Control.CanDeserialize(content))
				return Control.Deserialize(content, player, time);
			if(Deploy.CanDeserialize(content))
				return Deploy.Deserialize(content, player, time);
			if(MoveInBoard.CanDeserialize(content))
				return MoveInBoard.Deserialize(content, player, time);
			if(PickCard.CanDeserialize(content))
				return PickCard.Deserialize(content, player, time);
			//if(Procl
			//if(TakeInitiative
			if(TurnEnd.CanDeserialize(content))
				return TurnEnd.Deserialize(content, player, time);

			return null;
		}

		public override int GetHashCode() => Serialize().GetHashCode();
		public override bool Equals(object other)
		{
			if (other is Action a)
				return Equals(a);
			return false;
		}

		public bool Equals(Action other) => Serialize().Equals(other.Serialize()) && Player.PlayerId == other.Player.PlayerId && Time.Equals(other.Time);
	}

	public abstract class Action<T> : Action
		where T : Action<T>
	{
		protected Action(Gamer player, DateTime time) : base(player, time)
		{
		}

		protected static string GetClassName => typeof(T).Name;
		protected static string IdentifierString = GetClassName + "|";

		public static bool CanDeserialize(string content)
		{
			return content.StartsWith(IdentifierString);
		}
	}
}
