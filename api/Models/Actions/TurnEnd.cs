﻿using System;

namespace Api.Model.Actions
{
	public class TurnEnd : Action<TurnEnd>
	{
		public int PlayerId { get; }

		public TurnEnd(int playerId, Gamer player, DateTime time) : base(player, time)
		{
			PlayerId = playerId;
		}

		public override string Serialize() => IdentifierString + PlayerId;
		public static TurnEnd Deserialize(string content, Gamer player, DateTime time)
		{
			if(!CanDeserialize(content))
				return default;
			content = content.Split('|', 2)[1];

			return new TurnEnd(int.Parse(content), player, time);
		}
	}
}
