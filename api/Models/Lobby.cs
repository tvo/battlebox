﻿using System.Collections.Immutable;
using System.Collections.Generic;
using System;

namespace Api.Model
{
	public class Lobby
	{
		public readonly int Id;
		public readonly string Name;
		public readonly int RequiredPlayers;
		public readonly ImmutableList<int> TeamAPlayerIds;
		public readonly ImmutableList<int> TeamBPlayerIds;
		public DateTime CreatedDate;
		public int CreatedById;

		public Lobby(int id, string name, GameType gameType, IEnumerable<int> playerAIds, IEnumerable<int> playerBIds, DateTime createdDate, int createdById)
		{
			Id = id;
			Name = name ?? "";
			RequiredPlayers = gameType == GameType.FourPlayer ? 4 : 2;

			TeamAPlayerIds = ImmutableList<int>.Empty;
			if(playerAIds != null)
				TeamAPlayerIds = TeamAPlayerIds.AddRange(playerAIds);

			TeamBPlayerIds = ImmutableList<int>.Empty;
			if(playerBIds != null)
				TeamBPlayerIds = TeamBPlayerIds.AddRange(playerBIds);

			CreatedDate = createdDate;
			CreatedById = createdById;
		}
	}
}
