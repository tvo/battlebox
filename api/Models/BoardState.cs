﻿using System.Linq;
using System.Collections.Immutable;

namespace Api.Model
{
	using IPos = Positioning.IPosition;

	public class BoardState
	{
		public int TurnNumber { get; }
		public ImmutableList<CoinStack> CoinsOnBoard { get; }

		public BoardState (ImmutableList<CoinStack> coins)
		{
		}

		public BoardState Attack(IPos from, IPos to)
		{
			// initialize and make sure that the coins exist
			var toStack = GetStackOnPosition(to);
			if(to is null)
				return null;
			var fromStack = GetStackOnPosition(from);
			if(fromStack is null)
				return null;

			if(!fromStack.CanAttack(toStack))
				return null;

			var newToStack = new CoinStack(toStack.Coin, toStack.Amount - 1, to, toStack.Player);

			return new BoardState(CoinsOnBoard.Replace(toStack, newToStack));
		}

		public CoinStack GetStackOnPosition(IPos tile)
		{
			return CoinsOnBoard.FirstOrDefault(x => x.Position.Equals(tile));
		}

		public BoardState MoveStackToPosition(IPos from, IPos to)
		{
			var f = GetStackOnPosition(from);
			var t = GetStackOnPosition(to);
			if(f is null || t != null)
				return null;

			var newF = new CoinStack(f.Coin, f.Amount, to, f.Player);

			var coins = CoinsOnBoard.Replace(f, newF);

			return new BoardState(coins);
		}

		public static IPos NoPosition = null;
	}
}
