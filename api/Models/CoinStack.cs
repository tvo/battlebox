﻿#nullable enable
namespace Api.Model
{
	public class CoinStack
	{
		public readonly int Amount;
		public readonly CoinType Coin;
		public readonly Positioning.IPosition Position;
		public readonly Gamer Player;

		public CoinStack(CoinType coin, int amount, Positioning.IPosition position, Gamer player)
		{
			Coin = coin;
			Amount = amount;
			Position = position;
			Player = player;

			if(Position is null)
				Position = new Positioning.Cube(0, 0, 0);
		}

		public CoinStack Merge(CoinStack other) => new CoinStack(Coin, Amount + other.Amount, Position, Player);

		public bool CanAttack(CoinStack other)
		{
			// can only attack the other team
			if(Player.Team != other.Player.Team)
				return false;
			return Position.IsNeighbor(other.Position);
		}
	}
}
