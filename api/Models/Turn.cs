﻿using System.Collections.Immutable;
using System.Collections.Generic;

namespace Api.Model
{
	using Actions;

	public class Turn
	{
		public int PlayerId { get; }

		public int RoundNumber { get; } // increments when all players finish their turns.
		public int TurnNumber { get; } // each player has one per round
		public ImmutableList<Action> StateChanges { get; } = ImmutableList<Action>.Empty;

		public Turn(int playerId, int round, int turnNumber, IEnumerable<Action> stateUpdates)
		{
			PlayerId = playerId;
			RoundNumber = round;
			TurnNumber = turnNumber;
			if(stateUpdates != null)
				StateChanges = StateChanges.AddRange(stateUpdates);
		}

		// This cannot determine if the turn is in the same game or not.
		public bool SameTurnAs(Turn other)
		{
			return PlayerId == other.PlayerId && 
				RoundNumber == other.RoundNumber &&
				TurnNumber == other.TurnNumber;
		}

		public Turn AddStateChange(Action toAdd) => new Turn(PlayerId, RoundNumber, TurnNumber, StateChanges.Add(toAdd));
		public Turn StartNextTurn(int nextTurnsPlayerId) => new Turn(nextTurnsPlayerId, RoundNumber + 1, 0, null);
	}
}
