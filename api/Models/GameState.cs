﻿using System.Collections.Immutable;
using System.Linq;

namespace Api.Model
{
	public class PlayerCard
	{
		public int PlayerId { get; }
		public CoinType Card { get; }

		public PlayerCard(int playerId, CoinType card)
		{
			PlayerId = playerId;
			Card = card;
		}
	}

	public class GameState
	{
		public Game Game { get; }
		public PhaseEnum Phase { get; }
		public GameType PlayerNumber { get; }
		public int TurnNumber { get; }

		public BoardCoins CoinsOnBoard { get; }
		public ImmutableList<Gamer> Players { get; }
		public ImmutableList<PlayerCard> PlayerCards { get; } = ImmutableList<PlayerCard>.Empty;
		public ImmutableList<Decree> Decrees { get; } = ImmutableList<Decree>.Empty;

		public GameState (Game game, PhaseEnum phase, int turnNumber, BoardCoins coins = null, ImmutableList<PlayerCard> playerCards = null)
		{
			Game = game;

			if(game.Players.Count == 4)
				PlayerNumber = Model.GameType.FourPlayer;
			else if(game.Players.Count == 2)
				PlayerNumber = Model.GameType.TwoPlayer;
			else
				throw new System.NotImplementedException("only two or four player game types accepted.");

			// add lists. Set to empty if they are null.
			if(coins != null)
				CoinsOnBoard = coins;
			else
				CoinsOnBoard = new BoardCoins();

			if(playerCards != null)
				PlayerCards = playerCards;

			Phase = phase;
			Players = game.Players;
		}


		public GameState ApplyAction(Actions.Action action)
		{
			if(action is Actions.TurnEnd)
				return new GameState(Game, Phase, TurnNumber + 1, CoinsOnBoard, PlayerCards);
			if(action is Actions.PickCard pickCard)
			{
				var totalCardsPerDraftPhase = Game.TwoPlayerDrawOrder.Count;
				if(PlayerNumber == GameType.FourPlayer)
					totalCardsPerDraftPhase = Game.FourPlayerDrawOrder.Count;
				var nextPhase = totalCardsPerDraftPhase == PlayerCards.Count ? PhaseEnum.Playing : PhaseEnum.Drafting;

				var newCoin = new CoinStack(pickCard.CoinType, Actions.PickCard.StartingAmount[pickCard.CoinType] - 2, BoardState.NoPosition, pickCard.Player);
				var coinToBag = new CoinStack(pickCard.CoinType, 2, BoardState.NoPosition, pickCard.Player);
				var nextBoard = CoinsOnBoard.AddCoin(CoinLocation.Supply, newCoin);
				nextBoard = CoinsOnBoard.AddCoin(CoinLocation.Bag, coinToBag);

				return new GameState(Game, nextPhase, TurnNumber, nextBoard, PlayerCards.Add(new PlayerCard(pickCard.Player.PlayerId, pickCard.CoinType)));
			}
			if(action is Actions.Deploy deploy)
			{
				var nextBoard = CoinsOnBoard.MoveCoinToBoard(deploy.Location, deploy.CoinUsed, deploy.To, 1);
				return new GameState(Game, Phase, TurnNumber, nextBoard, PlayerCards);
			}
			if(action is Actions.MoveInBoard move)
			{
				var coinsInFrom = CoinsOnBoard.CoinsInBoard.FirstOrDefault(coin => coin.Position.Equals(move.From))?.Amount ?? 0;
				var nextBoard = CoinsOnBoard.MoveBoardCoin(move.From, move.To, coinsInFrom);
				return new GameState(Game, Phase, TurnNumber, nextBoard, PlayerCards);
			}
			if(action is Actions.Remove remove)
			{
				var coinsInFrom = CoinsOnBoard.CoinsInBoard.FirstOrDefault(coin => coin.Position.Equals(remove.From))?.Amount ?? 0;
				var nextBoard = CoinsOnBoard.MoveCoinFromBoard(remove.From, remove.To, coinsInFrom);
				return new GameState(Game, Phase, TurnNumber, nextBoard, PlayerCards);
			}
			if(action is Actions.Transfer transfer)
			{
				var nextBoard = CoinsOnBoard.MoveCoin_NoBoard(transfer.From, transfer.To, transfer.Type, transfer.Amount);
				return new GameState(Game, Phase, TurnNumber, nextBoard, PlayerCards);
			}

			throw new System.NotImplementedException("Error applying the action to the game state.");
		}
	}
}
