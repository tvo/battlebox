﻿namespace Api.Model
{
	public enum DecreeEnum
	{
		Enlist = 1,
		Guard = 2,
		March = 3,
		Redeploy = 4,
		Reinforce = 5,
		Sacrifice = 6,
		Spy = 7
	}
}
