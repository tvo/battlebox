﻿using System;
using System.Linq;
using System.Collections.Immutable;
using System.Collections.Generic;

namespace Api.Model
{
	using Positioning;

	public class Game
	{
		public int Id { get; }
		public string Name { get; }
		public GameType GameType { get; set; }
		public ImmutableList<Gamer> Players { get; }
		public DateTime CreatedDate { get; }
		public int CreatedById { get; }

		public int PlayerIdsTurn { get; }
		public PhaseEnum Phase { get; }

		public Game(int id, string name, IEnumerable<Gamer> players, DateTime createdDate, int createdById)
		{
			Id = id;
			Name = name ?? "";
			GameType = players.Count() == 4 ? GameType.FourPlayer : GameType.TwoPlayer;
			Players = ImmutableList<Gamer>.Empty;
			if(players != null)
				Players = Players.AddRange(players);
			CreatedDate = createdDate;
			CreatedById = createdById;
		}

		public bool ValidPlayers()
		{
			// count is correct.
			if (Players.Count != 2 && Players.Count != 4)
				return false;
			if (Players.Count != Players.Select(x => x.PlayerId).Distinct().Count())
				return false;
			if (!Players.All(x => x.GameId == Id))
				return false;

			return true;
		}

		public GameState ApplyState(IEnumerable<Actions.Action> actions)
		{
			var state = new GameState(this, PhaseEnum.Drafting, 0);
			foreach(var action in actions)
			{
				state = state.ApplyAction(action);
				if(state is null)
					break;
			}
			return state;
		}

		/* TODO: saving this for later.
		public Game SortPlayers()
		{
			if(Players.Count == 2)
				return new Game();
			var players = ImmutableList<Gamer>.Empty;
			players = players.Add(Players.First());
			// two player
		}
		*/

		public Gamer GetPlayer(int playerId)
		{
			return Players.FirstOrDefault(p => p.PlayerId == playerId);
		}

		static Game()
		{
			// Two player game setup.

			var cords = new List<Tuple<int, int>>();
			for(var x = -3; x <= 3; ++x)
			{
				for(var y = 3; y >= -3; --y)
				{
					cords.Add(Tuple.Create(x, y));
				}
			}

			var positions = new List<Cube>();
			foreach(var (x, y) in cords)
				positions.Add(new Cube(x, y, -x - y));

			var center = positions.First(p => p.X == 0 && p.Y == 0 && p.Z == 0);
			positions = positions.Where(p => p.DistanceTo(center) <= 3).ToList();
			var controlPositions = new [] {
				new Cube(1, 2, -3),
				new Cube(-1, -2, 3),
				new Cube(-2, 3, -1),
				new Cube(-3, 2, 1),
				new Cube(-1, 1, 0),
				new Cube(-2, 0, 2),
				new Cube(2, -3, 1),
				new Cube(1, -1, 0),
				new Cube(3, -2, -1),
				new Cube(2, 0, -2),
			}.ToList();

			var twoBoard = BoardPosition<Cube>.FromBoardAndControls(positions, controlPositions);

			// Four player game setup.
			var fourPlayerControls = new [] {
				new Cube(4, -1, -3),
				new Cube(5, -3, -2),
				new Cube(-5, 3, 3),
				new Cube(-4, 1, 1),
			};
			controlPositions.AddRange(fourPlayerControls);
			positions.AddRange(new []{
				// left side
				new Cube(-4, 3, 3),
				new Cube(-4, 2, 2),
				new Cube(-4, 1, 1),
				new Cube(-5, 3, 3),
				new Cube(-5, 2, 2),
				// right side
				new Cube(4, -3, -1),
				new Cube(4, -2, -2),
				new Cube(4, -1, -3),
				new Cube(5, -3, -2),
				new Cube(5, -2, -3),
			});
			var fourBoard = BoardPosition<Cube>.FromBoardAndControls(positions, controlPositions);

			var empty = ImmutableList<IBoardPosition>.Empty;
			TwoPlayerBoard = empty.AddRange(twoBoard.Select(x => x as IBoardPosition));
			FourPlayerBoard = empty.AddRange(fourBoard.Select(x => x as IBoardPosition));
		}

		public static readonly ImmutableList<IBoardPosition> TwoPlayerBoard;
		public static readonly ImmutableList<IBoardPosition> FourPlayerBoard;
		public static readonly ImmutableList<int> FourPlayerDrawOrder = ImmutableList<int>.Empty.AddRange(new []{ 0,1,2,3,3,2,1,0,1,2,3,0 });
		public static readonly ImmutableList<int> TwoPlayerDrawOrder = ImmutableList<int>.Empty.AddRange(new []{ 0,1,1,0,0,1,1,0 });

		public static ImmutableList<int> GetPlayerDrawOrder(ICollection<int> playerIds)
		{
			if(playerIds.Count == 2)
				return ImmutableList<int>.Empty.AddRange(TwoPlayerDrawOrder.Select(i => playerIds.ElementAt(i)));
			return ImmutableList<int>.Empty.AddRange(FourPlayerDrawOrder.Select(i => playerIds.ElementAt(i)));
		}
	}
}
