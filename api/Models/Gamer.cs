﻿using System.Collections.Immutable;
using System.Collections.Generic;

namespace Api.Model
{
	public class Gamer
	{
		public readonly int PlayerId;
		public readonly int GameId;
		public readonly int Order;
		public readonly Team Team;
		public readonly ImmutableList<CoinType> Coins = ImmutableList<CoinType>.Empty;

		public Gamer(int playerId, int gameId, Team team, IEnumerable<CoinType> coins, int order = 0)
		{
			PlayerId = playerId;
			GameId = gameId;
			Team = team;
			if(coins != null)
				Coins = Coins.AddRange(coins);
			Order = order;
		}
	}
}
