﻿namespace Api.Model
{
	public class Coin
	{
		public CoinType Type { get; }

		public Coin(CoinType coinType)
		{
			Type = coinType;
		}

		public string Serialize() =>  Newtonsoft.Json.JsonConvert.SerializeObject(this);

		public static Coin Parse(string content)
		{
			return Newtonsoft.Json.JsonConvert.DeserializeObject<Coin>(content);
		}

	}
}
