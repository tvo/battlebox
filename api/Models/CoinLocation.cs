﻿namespace Api.Model
{
	public enum CoinLocation
	{
		Bag = 0,
		Hand = 1,
		Discard = 2,
		Supply = 3,
		OutOfPlay = 4,
		Board = 5,
	}
}
