﻿namespace Api.Model
{
	public enum PhaseEnum
	{
		Drafting = 0,
		Playing = 1,
		Complete = 2,
	}
}
