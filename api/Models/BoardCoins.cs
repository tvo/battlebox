﻿using System.Linq;
using Api.Model.Positioning;
using Lst = System.Collections.Immutable.ImmutableList<Api.Model.CoinStack>;
using Enu = System.Collections.Generic.IEnumerable<Api.Model.CoinStack>;

namespace Api.Model
{
	public class BoardCoins
	{
		public readonly Lst CoinsInBag = Lst.Empty;
		public readonly Lst CoinsInHand = Lst.Empty;
		public readonly Lst CoinsInDiscard = Lst.Empty;
		public readonly Lst CoinsInSupply = Lst.Empty;
		public readonly Lst CoinsInOutOfPlay = Lst.Empty;
		public readonly Lst CoinsInBoard = Lst.Empty;

		public BoardCoins()
		{
		}

		private BoardCoins(Lst bag, Lst hand, Lst discard, Lst supply, Lst outOfPlay, Lst board)
		{
			CoinsInBag = bag;
			CoinsInHand = hand;
			CoinsInDiscard = discard;
			CoinsInSupply = supply;
			CoinsInOutOfPlay = outOfPlay;
			CoinsInBoard = board;
		}

		private BoardCoins(Enu bag, Enu hand, Enu discard, Enu supply, Enu outOfPlay, Enu board)
		{
			if(bag != null) CoinsInBag = CoinsInBag.AddRange(bag);
			if(hand != null) CoinsInHand = CoinsInHand.AddRange(hand);
			if(discard != null) CoinsInDiscard = CoinsInDiscard.AddRange(discard);
			if(supply != null) CoinsInSupply = CoinsInSupply.AddRange(supply);
			if(outOfPlay != null) CoinsInOutOfPlay = CoinsInOutOfPlay.AddRange(outOfPlay);
			if(board != null) CoinsInBoard = CoinsInBoard.AddRange(board);
		}

		private Lst GetCollectionByCoinLocation(CoinLocation location)
		{
			switch(location)
			{
				case CoinLocation.Bag:
					return CoinsInBag;
				case CoinLocation.Board:
					return CoinsInBoard;
				case CoinLocation.Discard:
					return CoinsInDiscard;
				case CoinLocation.Hand:
					return CoinsInHand;
				case CoinLocation.OutOfPlay:
					return CoinsInOutOfPlay;
				case CoinLocation.Supply:
					return CoinsInSupply;
				default:
					return null;
			}
		}

		public Lst MergeCoin(Lst source, CoinStack toAdd)
		{
			// TODO: add the player too? =>  && x.Player == toAdd.Player
			var existingCoin = source.FirstOrDefault(x => x.Coin == toAdd.Coin && toAdd.Position.Equals(x.Position));
			if(existingCoin is null)
				return source.Add(toAdd);
			var updated = toAdd.Merge(existingCoin);
			return source.Remove(existingCoin).Add(updated);
		}

		private BoardCoins UpdateCollection(CoinLocation location, Lst collectionToUpdate)
		{
			var bag = CoinsInBag;
			var board = CoinsInBoard;
			var discard = CoinsInDiscard;
			var hand = CoinsInHand;
			var outOfPlay = CoinsInOutOfPlay;
			var supply = CoinsInSupply;

			if(location == CoinLocation.Bag) bag = collectionToUpdate;
			else if(location == CoinLocation.Board) board = collectionToUpdate;
			else if(location == CoinLocation.Discard) discard = collectionToUpdate;
			else if(location == CoinLocation.Hand) hand = collectionToUpdate;
			else if(location == CoinLocation.OutOfPlay) outOfPlay = collectionToUpdate;
			else if(location == CoinLocation.Supply) supply = collectionToUpdate;
			else throw new System.NotImplementedException("Coin location not implemented.");

			return new BoardCoins(bag, hand, discard, supply, outOfPlay, board);
		}

		public BoardCoins AddCoin(CoinLocation location, CoinStack stack)
		{
			var collection = GetCollectionByCoinLocation(location);
			collection = MergeCoin(collection, stack);
			return UpdateCollection(location, collection);
		}

		private BoardCoins RemoveCoin(CoinLocation location, CoinType coinType, IPosition position, int amount = 1)
		{
			var collection = GetCollectionByCoinLocation(location);
			CoinStack coin;
			if(CoinLocation.Board == location)
				coin = collection.FirstOrDefault(x => x.Position.Equals(position));
			else
				coin = collection.FirstOrDefault(x => x.Coin == coinType);

			if(coin is null) return this;
			if(coin.Amount <= amount)
				return UpdateCollection(location, collection.Remove(coin));

			collection = collection.Remove(coin);
			coin = new CoinStack(coin.Coin, coin.Amount - amount, coin.Position, coin.Player);
			collection = collection.Add(coin);

			return UpdateCollection(location, collection);
		}

		public BoardCoins MoveCoin_NoBoard(CoinLocation fromLocation, CoinLocation toLocation, CoinType coinType, int amount)
		{
			var fromCollection = GetCollectionByCoinLocation(fromLocation);

			var player = fromCollection.First(x => x.Coin == coinType).Player;
			var board = RemoveCoin(fromLocation, coinType, null, amount);
			return board.AddCoin(toLocation, new CoinStack(coinType, amount, null, player));
		}

		public BoardCoins MoveBoardCoin(IPosition fromPosition, IPosition toPosition, int amount)
		{
			if(fromPosition.Equals(toPosition))
				return this;

			System.Console.WriteLine(fromPosition);
			var coin = CoinsInBoard.First(x => x.Position.Equals(fromPosition));
			var player = coin.Player;
			var coinType = coin.Coin;

			var board = RemoveCoin(CoinLocation.Board, coinType, fromPosition, amount);
			return board.AddCoin(CoinLocation.Board, new CoinStack(coinType, amount, toPosition, player));
		}

		public BoardCoins MoveCoinToBoard(CoinLocation fromLocation, CoinType fromType, IPosition toPosition, int amount)
		{
			var fromCollection = GetCollectionByCoinLocation(fromLocation);

			var coin = fromCollection.First(x => x.Coin == fromType);
			var player = coin.Player;
			var coinType = coin.Coin;

			// TODO: Validation - should flatten the amount, unless the total coin amount is not conserved.

			var board = RemoveCoin(fromLocation, coinType, toPosition, amount);
			return board.AddCoin(CoinLocation.Board, new CoinStack(coinType, amount, toPosition, player));
		}

		public BoardCoins MoveCoinFromBoard(IPosition from, CoinLocation toLocation, int amount)
		{
			var toCollection = GetCollectionByCoinLocation(toLocation);

			var coin = CoinsInBoard.First(x => x.Position.Equals(from));
			var player = coin.Player;
			var coinType = coin.Coin;

			var board = RemoveCoin(CoinLocation.Board, coinType, from, amount);
			return board.AddCoin(toLocation, new CoinStack(coinType, amount, null, player));
		}
	}
}
