﻿using System;

namespace Api.Model
{
	public class Player
	{
		public readonly int Id;
		public readonly string Name;

		public Player(int id, string name)
		{
			Id = id;
			Name = name ?? "";
		}

		// extra properties
		public DateTime Time { get; set; }
		public string Sub { get; set; }
		public string PictureUrl { get; set; }
		public string Nickname { get; set; }
		public string Locale { get; set; }

	}
}
