﻿using System.Collections.Immutable;
using System.Collections.Generic;
using System.Linq;

namespace Api.Model.Positioning
{
	public class BoardPosition<T> : IBoardPosition
		where T : IPosition
	{
		public readonly T Position;
		public bool Control { get; }

		public BoardPosition(T position, bool control)
		{
			Position = position;
			Control = control;
		}

		public int DistanceTo(IPosition otherPosition) => Position.DistanceTo(otherPosition);
		public bool IsNeighbor(IPosition other) => Position.IsNeighbor(other);
		public IPosition RotateRight() => Position.RotateRight();
		public ImmutableList<IPosition> DrawLineTo(IPosition end) => Position.DrawLineTo(end);

		public override int GetHashCode() => Position.GetHashCode() ^ Control.GetHashCode();
		public override string ToString() => Position.ToString();

		public bool Equals(IPosition other) => Position.Equals(other);
		public bool Equals(IBoardPosition other)
		{
			return Position.Equals(other) && Control == other.Control;
		}

		public static ImmutableList<BoardPosition<T>> FromBoardAndControls(IEnumerable<T> board, IEnumerable<IPosition> controlLocations)
		{
			var controls = new HashSet<IPosition>();
			controlLocations.ToList().ForEach(c => controls.Add(c));

			var result = ImmutableList<BoardPosition<T>>.Empty;

			return result.AddRange(
					board.Select(b => new BoardPosition<T>(b, controls.Contains(b)))
					);
		}
	}
}
