﻿using System;
using System.Collections.Immutable;

namespace Api.Model.Positioning
{
	public interface IPosition : IEquatable<IPosition>
	{
		int DistanceTo(IPosition other);
		bool IsNeighbor(IPosition other);
		ImmutableList<IPosition> DrawLineTo(IPosition end);
		IPosition RotateRight();
	}
}
