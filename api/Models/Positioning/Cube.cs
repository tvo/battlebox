﻿using System;
using System.Collections.Immutable;

namespace Api.Model.Positioning
{
	public class Cube : IPosition
	{
		public readonly int X, Y, Z;

		public Cube(int x, int y, int z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public int DistanceTo(IPosition otherPosition)
		{
			var other = ConverToCube(otherPosition);

			int abs(int num) => System.Math.Abs(num);
			return (abs(X - other.X) + abs(Y - other.Y) + abs(Z - other.Z)) / 2;
		}

		public bool IsNeighbor(IPosition other) => DistanceTo(other) == 1;

		public IPosition RotateRight()
		{
			return new Cube(-Z, -X, -Y);
		}

		public ImmutableList<IPosition> DrawLineTo(IPosition end)
		{
			throw new NotImplementedException();
		}

		public bool Equals(IPosition other)
		{
			if(DistanceTo(other) == 0)
				return true;
			return false;
		}

		public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode() ^ Z.GetHashCode();
		public int GetHashCode(IPosition position) => ConverToCube(position).GetHashCode();

		public static Cube ConverToCube(IPosition position)
		{
			if(position is Cube cube)
				return cube;
			return null;
		}

		public static Cube Parse(string content)
		{
			if(string.IsNullOrWhiteSpace(content))
				return null;
			var split = content.Split(",", 3);
			var x = int.Parse(split[0]);
			var y = int.Parse(split[1]);
			var z = int.Parse(split[2]);
			return new Cube(x, y, z);
		}

		public override string ToString() => $"{X},{Y},{Z}";

		private ImmutableList<Cube> Neighbors()
		{
			return ImmutableList<Cube>.Empty.AddRange(new []{
					new Cube(X + 1, Y - 1, Z),
					new Cube(X + 1, Y, Z - 1),
					new Cube(X, Y + 1, Z - 1),
					new Cube(X - 1, Y + 1, Z),
					new Cube(X - 1, Y, Z + 1),
					new Cube(X, Y - 1, Z + 1),
					});
		}

		public static ImmutableList<IPosition> CreateRing(ushort radius)
		{
			var result = ImmutableList<IPosition>.Empty;
			var start = new Cube(- radius, 0, radius);
			result = result.Add(start);

			if(radius == 0)
				return result;

			var last = start;
			for(var i = 0; i < 6; ++i)
			{
				for(var r = 0; r <= radius; ++r)
				{
					last = last.Neighbors()[i];
					result = result.Add(last);
				}
			}
			return result;
		}
	}
}
