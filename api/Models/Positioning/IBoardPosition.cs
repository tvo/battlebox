﻿using System;

namespace Api.Model.Positioning
{
	public interface IBoardPosition : IPosition, IEquatable<IBoardPosition>
	{
		bool Control { get; }
	}
}
