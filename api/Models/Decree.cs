﻿namespace Api.Model
{
	public class Decree
	{
		public readonly DecreeEnum Type;
		public readonly int TeamWolfUses;
		public readonly int TeamRavenUses;

		public Decree(DecreeEnum type, int wolfCount, int ravenCount)
		{
			Type = type;
			TeamWolfUses = wolfCount;
			TeamRavenUses = ravenCount;
		}
	}
}
