﻿namespace Api.Model
{
	public enum CoinType
	{
		FaceDown = 0,
		Archer = 1,
		Bannerman = 2,
		Berserker = 3,
		Bishop = 4,
		Cavalry = 5,
		Crossbowman = 6,
		Earl = 7,
		Ensign = 8,
		Footman = 9,
		Herald = 10,
		Knight = 11,
		Lancer = 12,
		LightCavalry = 13,
		Marshall = 14,
		Mercenary = 15,
		Pikeman = 16,
		RoyalGuard = 17,
		Scout = 18,
		Swordsman = 19,
		WarriorPriest = 20,

		// TODO: royal coin
	}
}
