﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using AutoMapper;

namespace Api
{
	using Auth;
	using db;

	public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddTransient<Repositories.Lobby.ILobbyRepository, Repositories.Lobby.LobbyRepository>();
			services.AddTransient<Repositories.Player.IPlayerRepository, Repositories.Player.PlayerRepository>();
			services.AddTransient<Repositories.Game.IGameRepository, Repositories.Game.GameRepository>();
			services.AddTransient<Repositories.Action.ITurnRepository, Repositories.Action.TurnRepository>();
			services.AddTransient<Facades.LobbyFacade>();
			services.AddTransient<Facades.PlayerFacade>();
			services.AddTransient<Facades.GameFacade>();
			services.AddTransient<Facades.TurnFacade>();

			services.AddTransient<Services.IUserInfoProvider, Services.UserInfoProvider>();

			services.AddHttpContextAccessor();
			services.AddAutoMapper(typeof(Map));
			var sql = Configuration["sql"];
			services.AddDbContext<DataContext>(options => options.UseNpgsql(sql));

			services.AddCors(options =>
					{
					options.AddDefaultPolicy(
							builder => builder.AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader());
					});
			Microsoft.IdentityModel.Logging.IdentityModelEventSource.ShowPII = true;
			var domain = Configuration["domain"]; // $"https://dev-85pyx5-d.auth0.com/";
			var audience = Configuration["audience"]; // "battlebox.tvonsegg.com";
			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(options =>
			{
				options.Authority = domain;
				options.Audience = audience;
			});
			services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();
			services.AddAuthorization(options =>
					{
					//options.AddPolicy("read:messages", policy => policy.Requirements.Add(new HasScopeRequirement("read:messages", domain)));
					options.AddPolicy("read:messages", policy => policy.RequireRole("user"));
					});

			services.AddSwaggerGen(c =>
					{
					c.SwaggerDoc("v1", new OpenApiInfo { Title = "Battle Box", Version = "v1" });
					c.OperationFilter<PlayerIdInHeaderFilter>();
					});

			// Register the scope authorization handler
			services.AddControllers()
				.AddNewtonsoftJson();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (true)
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				//app.UseHsts();
			}

			app.UseSwagger(x => {
					x.RouteTemplate = "api/swagger/{documentName}/swagger.json";
					});
			app.UseSwaggerUI(config =>
					{
					config.SwaggerEndpoint("v1/swagger.json", "Battle Box");
					config.RoutePrefix = "api/swagger";
					});

			app.UseHttpsRedirection();

			app.UseCors();
			app.UseAuthentication();
			app.UseRouting();
			app.UseAuthorization(); // for some reason this should be between use routing and use endpoints.
			app.UseEndpoints(endpoints => endpoints.MapControllers());
		}
	}
}
