﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
// using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
	using Facades;

	[Route("/api/lobby")]
	// [Authorize]
	public class LobbyController : ControllerBase
	{
		private readonly LobbyFacade facade;

		public LobbyController(LobbyFacade facade)
		{
			this.facade = facade;
		}

		[HttpGet]
		public async Task<ActionResult> GetAll()
		{
			return Ok(await facade.GetAll());
		}

		[HttpGet("{lobbyId}")]
		public async Task<ActionResult> Get(int lobbyId)
		{
			var lobby = await facade.Get(lobbyId);
			if (lobby is null)
				return NotFound(lobbyId);
			return Ok(lobby);
		}

		[HttpPost]
		public async Task<ActionResult> Create([FromBody]dto.LobbyCreate lobby)
		{
			var created = await facade.Create(lobby);
			return Created("/api/lobby/" + created.Id, created);
		}

		[HttpPost("{lobbyId}/join/{team}")]
		public async Task<ActionResult> Join(int lobbyId, Model.Team team)
		{
			if(await facade.Join(lobbyId, team))
				return Ok(await facade.Get(lobbyId));
			return new BadRequestResult();
		}

		[HttpPost("{lobbyId}/leave")]
		public async Task<ActionResult> Leave(int lobbyId)
		{
			if(await facade.Leave(lobbyId))
				return Ok(await facade.Get(lobbyId));
			return new BadRequestResult();
		}

		[HttpDelete("{lobbyId}")]
		public async Task<ActionResult> Delete(int lobbyId)
		{
			if(await facade.Delete(lobbyId))
				return Ok(new dto.DeletedDto{ Id = lobbyId });
			return new BadRequestResult();
		}
	}
}
