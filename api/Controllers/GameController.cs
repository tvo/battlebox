﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
// using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
	using Facades;

	[Route("api/game")]
	// [Authorize]
	[ApiController]
	public class GameController : ControllerBase
	{
		private readonly GameFacade facade;

		public GameController(GameFacade facade)
		{
			this.facade = facade;
		}

		[HttpGet]
		public async Task<ActionResult> GetAll()
		{
			var games = await facade.GetAll();
			if (games is null)
				return BadRequest();

			return Ok(games);
		}

		[HttpGet("{id}")]
		public async Task<ActionResult> GetOne(int id)
		{
			var game = await facade.GetOne(id);
			if(game is null)
				return BadRequest();

			return Ok(game);
		}

		[HttpDelete("{id}")]
		public async Task<ActionResult> Delete(int id)
		{
			var game = await facade.Delete(id);
			if(game is null)
				return BadRequest();

			return Ok(game);
		}

		[HttpPost]
		public async Task<ActionResult> Create([FromBody]Api.dto.GameCreate game)
		{
			var lobbyDto = new dto.LobbyCreate();

			var created = await facade.Create(game);
			if (created is null)
				return BadRequest();
			return Created("/api/game/" + created.Id, created);
		}


		[HttpGet("board/2")]
		public ActionResult BoardPositionsTwo([FromServices] IMapper mapper)
		{
			return Ok(mapper.Map<List<dto.HexPosition>>(Model.Game.TwoPlayerBoard));
		}

		[HttpGet("board/4")]
		public ActionResult BoardPositionsFour([FromServices] IMapper mapper)
		{
			return Ok(mapper.Map<List<dto.HexPosition>>(Model.Game.FourPlayerBoard));
		}
	}
}
