﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
// using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
	using Facades;
	
	[Route("/api/player")]
	// [Authorize]
	public class PlayerController : ControllerBase
	{
		private readonly PlayerFacade facade;

		public PlayerController(PlayerFacade facade)
		{
			this.facade = facade;
		}

		[HttpGet("me")]
		public async Task<ActionResult> Me()
		{
			return Ok(await facade.Me());
		}

		[HttpPost]
		public async Task<ActionResult> Register()
		{
			return Ok(await facade.Register(0));
		}
	}
}
