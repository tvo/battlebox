﻿using Microsoft.AspNetCore.Mvc;

namespace WebAPIApplication.Controllers
{
	[Route("api")]
	[ApiController]
	public class ApiController : ControllerBase
	{
		[HttpGet("version")]
		public IActionResult GetVersion()
		{
			return Ok(new { version = "1.0.0"});
		}
	}
}
