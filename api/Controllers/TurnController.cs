﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
// using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
	using Facades;
	using Services;

	[Route("/api/game/{gameId}")]
	// [Authorize]
	public class TurnController : ControllerBase
	{
		private readonly TurnFacade facade;

		public TurnController(TurnFacade facade)
		{
			this.facade = facade;
		}

		[HttpPost("turn/end")]
		public async Task<IActionResult> EndTurn(int gameId)
		{
			return (await facade.EndTurn(gameId)).GetResponse();
		}

		[HttpPost("turn/deploy")]
		public async Task<IActionResult> Deploy(int gameId, [FromBody]dto.DeployDto dto)
		{
			return (await facade.Deploy(gameId, dto)).GetResponse();
		}

		[HttpPost("turn/move")]
		public async Task<IActionResult> Move(int gameId, [FromBody]dto.MoveDto dto)
		{
			return (await facade.Move(gameId, dto)).GetResponse();
		}

		[HttpPost("turn/remove")]
		public async Task<IActionResult> Remove(int gameId, [FromBody]dto.RemoveDto dto)
		{
			return (await facade.Remove(gameId, dto)).GetResponse();
		}

		[HttpPost("turn/transfer")]
		public async Task<IActionResult> Transfer(int gameId, [FromBody]dto.TransferDto dto)
		{
			return (await facade.Transfer(gameId, dto)).GetResponse();
		}

		[HttpGet("turn/history")]
		public async Task<ActionResult> History(int gameId)
		{
			var history = await facade.History(gameId);
			if (history is null)
				return BadRequest();
			return Ok(history);
		}

		[HttpGet("state")]
		public async Task<ActionResult> State(int gameId)
		{
			var game = await facade.GetState(gameId);
			if(game is null)
				return BadRequest();

			return Ok(game);
		}

		[HttpPost("requestDraw")]
		public async Task<ActionResult> RequestDraw(int gameId, [FromBody] Model.CoinType draw)
		{
			return (await facade.PickCard(gameId, draw)).GetResponse();
		}

		[HttpGet("availableCards")]
		public async Task<ActionResult> AvailableCards(int gameId)
		{
			var cards = await facade.GetAvailableCards(gameId);
			if(cards is null)
				return BadRequest();
			return Ok(cards);
		}

	}
}
