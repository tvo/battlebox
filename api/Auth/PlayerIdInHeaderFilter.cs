﻿using System.Collections.Generic;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.OpenApi.Models;

namespace Api.Auth
{
	public class PlayerIdInHeaderFilter : IOperationFilter
	{
		public void Apply(OpenApiOperation operation, OperationFilterContext context)
		{
			if (operation.Parameters == null)
				operation.Parameters = new List<OpenApiParameter>();

			operation.Parameters.Add(new OpenApiParameter
					{
					Name = "playerId",
					In = ParameterLocation.Header,
					Required = true // set to false if this is optional
					});
		}
	}
}
