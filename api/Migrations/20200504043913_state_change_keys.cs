﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Api.Migrations
{
    public partial class state_change_keys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StateChanges",
                table: "StateChanges");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StateChanges",
                table: "StateChanges",
                columns: new[] { "GameId", "ChangeNumber", "TurnNumber", "RoundNumber" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StateChanges",
                table: "StateChanges");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StateChanges",
                table: "StateChanges",
                columns: new[] { "GameId", "ChangeNumber" });
        }
    }
}
