﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Api.Migrations
{
    public partial class game_state_updates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionNumber",
                table: "StateChanges");

            migrationBuilder.AddColumn<int>(
                name: "Phase",
                table: "Games",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PlayerOrder",
                table: "Games",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlayersTurn",
                table: "Games",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CardFour",
                table: "Gamers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_StateChanges_Games_GameId",
                table: "StateChanges",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StateChanges_Games_GameId",
                table: "StateChanges");

            migrationBuilder.DropColumn(
                name: "Phase",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "PlayerOrder",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "PlayersTurn",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "CardFour",
                table: "Gamers");

            migrationBuilder.AddColumn<int>(
                name: "ActionNumber",
                table: "StateChanges",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
